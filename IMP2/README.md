# IMP2 --- Simple Program Verification in Isabelle/HOL

IMP2 is a simple imperative language together with Isabelle tooling to 
create a program verification environment in Isabelle/HOL.
The tools include a C-like syntax, a verification condition generator, 
and Isabelle commands for the specification of programs.

This experiment shows how to use Isabelle as an environment for verifying 
imperative programs. While this entry is limited to a simple imperative language,
the ideas could be extended to more ambitious languages.

The abstract syntax and semantics are very simple and well-documented. 
They are suitable to be used in a course, as extension to the IMP language 
which comes with the Isabelle distribution.


## IMP2 Language Features
  * While-language with recursive procedures
  * Local and global variables, parameter passing via globals 
  * Arrays and integer variables
  * Small-step and big-step semantics
  * Very simple and well-documented abstract syntax and semantics
  
## Tools
  * Verification condition generator
  * Isabelle setup to parse a C-like syntax
  * Isabelle commands to define and prove programs   
  
## Getting Started
  The doc subdirectory contains a Quickstart guide and a large collection of example verifications.
  


