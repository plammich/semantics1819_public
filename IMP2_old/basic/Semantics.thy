section \<open>Semantics of IMP\<close>
theory Semantics
imports Syntax "HOL-Eisbach.Eisbach_Tools" 
begin

subsection \<open>State\<close>
type_synonym state = "vname \<Rightarrow> val"

definition null_state ("<>") where
  "null_state \<equiv> \<lambda>x. \<lambda>i. 0"
syntax 
  "_State" :: "updbinds => 'a" ("<_>")
translations
  "_State ms" == "_Update <> ms"
  "_State (_updbinds b bs)" <= "_Update (_State b) bs"


definition combine_states :: "state \<Rightarrow> state \<Rightarrow> state" ("<_|_>" [0,0] 1000)
  where "<s|t> n = (if is_local n then s n else t n)"

context notes [simp] = combine_states_def begin

lemma combine_collapse: "<s|s> = s" by auto

lemma combine_nest:  
  "<s|<s'|t>> = <s|t>"
  "<<s|t'>|t> = <s|t>"
  by auto  
  
lemma combine_query: 
  "is_local x \<Longrightarrow> <s|t> x = s x"  
  "is_global x \<Longrightarrow> <s|t> x = t x"  
  by auto

lemma combine_upd: 
  "is_local x \<Longrightarrow> <s|t>(x:=v) = <s(x:=v)|t>"  
  "is_global x \<Longrightarrow> <s|t>(x:=v) = <s|t(x:=v)>"  
  by auto  
  
lemma combine_cases[cases type]:
  obtains l g where "s = <l|g>"
  by (fastforce)
  
end  
  
  
  
    
  
  
subsection \<open>Arithmetic Expressions\<close>

fun aval :: "aexp \<Rightarrow> state \<Rightarrow> pval" where
  "aval (N n) s = n" 
| "aval (Vidx x i) s = s x (aval i s)" 
| "aval (Unop f a\<^sub>1) s = f (aval a\<^sub>1 s)"
| "aval (Binop f a\<^sub>1 a\<^sub>2) s = f (aval a\<^sub>1 s) (aval a\<^sub>2 s)"

subsection \<open>Boolean Expressions\<close>

fun bval :: "bexp \<Rightarrow> state \<Rightarrow> bool" where
  "bval (Bc v) s = v" 
| "bval (Not b) s = (\<not> bval b s)" 
| "bval (BBinop f b\<^sub>1 b\<^sub>2) s = f (bval b\<^sub>1 s) (bval b\<^sub>2 s)" 
| "bval (Cmpop f a\<^sub>1 a\<^sub>2) s = f (aval a\<^sub>1 s) (aval a\<^sub>2 s)"

subsection \<open>Big-Step Semantics\<close>

text \<open>Note: This semantics gets stuck on calling a non-existing procedure.
  This is no problem when showing total correctness, 
  but for partial correctness, we would have to distinguish between 
  non-termination and undefined procedure.
\<close>

inductive
  big_step :: "program \<Rightarrow> com \<times> state \<Rightarrow> state \<Rightarrow> bool" ("_: _ \<Rightarrow> _" [1000,55,55] 55)
where
  Skip: "\<pi>:(SKIP,s) \<Rightarrow> s" 
| AssignIdx: "\<pi>:(x[i] ::= a,s) \<Rightarrow> s(x := (s x)(aval i s := aval a s))" 
| ArrayCpy: "\<pi>:(x[] ::= y,s) \<Rightarrow> s(x := s y)" 
| ArrayClear: "\<pi>:(CLEAR x[],s) \<Rightarrow> s(x := (\<lambda>_. 0))" 
| Seq: "\<lbrakk> \<pi>:(c\<^sub>1,s\<^sub>1) \<Rightarrow> s\<^sub>2;  \<pi>:(c\<^sub>2,s\<^sub>2) \<Rightarrow> s\<^sub>3 \<rbrakk> \<Longrightarrow> \<pi>:(c\<^sub>1;;c\<^sub>2, s\<^sub>1) \<Rightarrow> s\<^sub>3" 
| IfTrue: "\<lbrakk> bval b s; \<pi>:(c\<^sub>1,s) \<Rightarrow> t \<rbrakk> \<Longrightarrow> \<pi>:(IF b THEN c\<^sub>1 ELSE c\<^sub>2, s) \<Rightarrow> t" 
| IfFalse: "\<lbrakk> \<not>bval b s;  \<pi>:(c\<^sub>2,s) \<Rightarrow> t \<rbrakk> \<Longrightarrow> \<pi>:(IF b THEN c\<^sub>1 ELSE c\<^sub>2, s) \<Rightarrow> t" 
| Scope: "\<lbrakk> \<pi>:(c,<<>|s>) \<Rightarrow> s' \<rbrakk> \<Longrightarrow> \<pi>:(SCOPE c, s) \<Rightarrow> <s|s'>"
| WhileFalse: "\<not>bval b s \<Longrightarrow> \<pi>:(WHILE b DO c,s) \<Rightarrow> s" 
| WhileTrue: "\<lbrakk> bval b s\<^sub>1;  \<pi>:(c,s\<^sub>1) \<Rightarrow> s\<^sub>2;  \<pi>:(WHILE b DO c, s\<^sub>2) \<Rightarrow> s\<^sub>3 \<rbrakk> 
    \<Longrightarrow> \<pi>:(WHILE b DO c, s\<^sub>1) \<Rightarrow> s\<^sub>3"
| PCall: "\<lbrakk> \<pi> p = Some c; \<pi>:(c,s) \<Rightarrow> t \<rbrakk> \<Longrightarrow> \<pi>:(PCall p,s) \<Rightarrow> t"    
| PScope: "\<lbrakk> \<pi>' p = Some c; \<pi>':(c,s) \<Rightarrow> t \<rbrakk> \<Longrightarrow> \<pi>:(PScope \<pi>' p, s) \<Rightarrow> t"

subsubsection \<open>Proof Automation\<close>    
    
declare big_step.intros [intro]
lemmas big_step_induct[induct set] = big_step.induct[split_format(complete)]

inductive_simps Skip_simp: "\<pi>:(SKIP,s) \<Rightarrow> t"
inductive_simps AssignIdx_simp: "\<pi>:(x[i] ::= a,s) \<Rightarrow> t"
inductive_simps ArrayCpy_simp: "\<pi>:(x[] ::= y,s) \<Rightarrow> t"
inductive_simps ArrayInit_simp: "\<pi>:(CLEAR x[],s) \<Rightarrow> t"

inductive_simps Seq_simp: "\<pi>:(c1;;c2,s1) \<Rightarrow> s3"
inductive_simps If_simp: "\<pi>:(IF b THEN c1 ELSE c2,s) \<Rightarrow> t"
inductive_simps Scope_simp: "\<pi>:(SCOPE c,s) \<Rightarrow> t"
inductive_simps PCall_simp: "\<pi>:(PCall p,s) \<Rightarrow> t"
inductive_simps PScope_simp: "\<pi>:(PScope \<pi>' p,s) \<Rightarrow> t"

lemmas big_step_simps = 
  Skip_simp AssignIdx_simp ArrayCpy_simp ArrayInit_simp
  Seq_simp If_simp Scope_simp PCall_simp PScope_simp

  
inductive_cases SkipE[elim!]: "\<pi>:(SKIP,s) \<Rightarrow> t"
inductive_cases AssignIdxE[elim!]: "\<pi>:(x[i] ::= a,s) \<Rightarrow> t"
inductive_cases ArrayCpyE[elim!]: "\<pi>:(x[] ::= y,s) \<Rightarrow> t"
inductive_cases ArrayInitE[elim!]: "\<pi>:(CLEAR x[],s) \<Rightarrow> t"

inductive_cases SeqE[elim!]: "\<pi>:(c1;;c2,s1) \<Rightarrow> s3"
inductive_cases IfE[elim!]: "\<pi>:(IF b THEN c1 ELSE c2,s) \<Rightarrow> t"
inductive_cases ScopeE[elim!]: "\<pi>:(SCOPE c,s) \<Rightarrow> t"
inductive_cases PCallE[elim!]: "\<pi>:(PCall p,s) \<Rightarrow> t"
inductive_cases PScopeE[elim!]: "\<pi>:(PScope \<pi>' p,s) \<Rightarrow> t"
  
inductive_cases WhileE[elim]: "\<pi>:(WHILE b DO c,s) \<Rightarrow> t"
                             

subsubsection \<open>Automatic Derivation\<close>
  (* Testing the programs by constructing big-step derivations automatically *)    
  
  (* This rule is used to enforce simplification of the newly generated state, before passing it on *)
  lemma Assign': "s' = s(x := (s x)(aval i s := aval a s)) \<Longrightarrow> \<pi>:(x[i] ::= a, s) \<Rightarrow> s'" by auto
  lemma ArrayCpy': "s' = s(x := (s y)) \<Longrightarrow> \<pi>:(x[] ::= y, s) \<Rightarrow> s'" by auto
  lemma ArrayClear': "s' = s(x := (\<lambda>_. 0)) \<Longrightarrow> \<pi>:(CLEAR x[], s) \<Rightarrow> s'" by auto
  
  method big_step = 
    rule Skip Seq 
  | (rule Assign' ArrayCpy' ArrayClear', (simp;fail)) 
  | (rule IfTrue IfFalse WhileTrue WhileFalse), (simp;fail)
  (*| (simp only: While_Annot_def)*)
      
  (* TODO: Make this work nicely with scopes! And Calls!*)
  
  
  schematic_goal "Map.empty: (
    ''a'' ::= N 1;;
    WHILE Cmpop (\<lambda>x y. y < x) (V ''n'') (N 0) DO (
      ''a'' ::= Binop (+) (V ''a'') (V ''a'');; 
      ''n'' ::= Binop (-) (V ''n'') (N 1)
    ),<''n'':=(\<lambda>_. 5)>) \<Rightarrow> ?s"  
    by big_step+



subsection \<open>Command Equivalence\<close>

definition
  equiv_c :: "com \<Rightarrow> com \<Rightarrow> bool" (infix "\<sim>" 50) where
  "c \<sim> c' \<equiv> (\<forall>\<pi> s t. \<pi>:(c,s) \<Rightarrow> t  = \<pi>:(c',s) \<Rightarrow> t)"

lemma equivI[intro?]: "\<lbrakk>
  \<And>s t \<pi>. \<pi>:(c,s) \<Rightarrow> t \<Longrightarrow> \<pi>:(c',s) \<Rightarrow> t; 
  \<And>s t \<pi>. \<pi>:(c',s) \<Rightarrow> t \<Longrightarrow> \<pi>:(c,s) \<Rightarrow> t\<rbrakk> 
  \<Longrightarrow> c \<sim> c'"  
  by (auto simp: equiv_c_def)
  
lemma equivD[dest]: "c \<sim> c' \<Longrightarrow> \<pi>:(c,s) \<Rightarrow> t \<longleftrightarrow> \<pi>:(c',s) \<Rightarrow> t"  
  by (auto simp: equiv_c_def)

text \<open>Command equivalence is an equivalence relation, i.e.\ it is
reflexive, symmetric, and transitive.\<close>

lemma equiv_refl[simp, intro!]:  "c \<sim> c" by (blast intro: equivI)
lemma equiv_sym[sym]:   "(c \<sim> c') \<Longrightarrow> (c' \<sim> c)" by (blast intro: equivI)
lemma equiv_trans[trans]: "c \<sim> c' \<Longrightarrow> c' \<sim> c'' \<Longrightarrow> c \<sim> c''" by (blast intro: equivI)

subsubsection \<open>Basic Equivalences\<close>
lemma while_unfold:
  "(WHILE b DO c) \<sim> (IF b THEN c;; WHILE b DO c ELSE SKIP)"
  by rule auto

lemma triv_if:
  "(IF b THEN c ELSE c) \<sim> c"
by (auto intro!: equivI)

lemma commute_if:
  "(IF b1 THEN (IF b2 THEN c11 ELSE c12) ELSE c2) 
   \<sim> 
   (IF b2 THEN (IF b1 THEN c11 ELSE c2) ELSE (IF b1 THEN c12 ELSE c2))"
by (auto intro!: equivI)

lemma sim_while_cong_aux:
  "\<lbrakk>\<pi>:(WHILE b DO c,s) \<Rightarrow> t; bval b = bval b'; c \<sim> c' \<rbrakk> \<Longrightarrow> \<pi>:(WHILE b' DO c',s) \<Rightarrow> t"
  apply(induction "WHILE b DO c" s t arbitrary: b c rule: big_step_induct)
  apply auto
  done

lemma sim_while_cong: "bval b = bval b' \<Longrightarrow> c \<sim> c' \<Longrightarrow> WHILE b DO c \<sim> WHILE b' DO c'"
  using equiv_c_def sim_while_cong_aux by auto

subsection \<open>Execution is Deterministic\<close>

text \<open>This proof is automatic.\<close>

theorem big_step_determ: "\<lbrakk> \<pi>:(c,s) \<Rightarrow> t; \<pi>:(c,s) \<Rightarrow> u \<rbrakk> \<Longrightarrow> u = t"
proof (induction arbitrary: u rule: big_step.induct)
  case (WhileTrue b s\<^sub>1 c s\<^sub>2 s\<^sub>3)
    then show ?case by blast
qed fastforce+

subsection \<open>Weakest Precondition\<close>  

context
  fixes \<pi> :: program
begin
  
  text \<open>Weakest precondition: 
    \<open>c\<close> terminates with a state that satisfies \<open>Q\<close>, when started from \<open>s\<close>\<close>
  definition "wp c Q s \<equiv> \<exists>t. \<pi>: (c,s) \<Rightarrow> t \<and> Q t"
    \<comment> \<open>Note that this definition exploits that the semantics is deterministic! 
      In general, we must ensure absence of infinite executions\<close>
    
  text \<open>Weakest liberal precondition: 
    If \<open>c\<close> terminates when started from \<open>s\<close>, the new state satisfies \<open>Q\<close>.\<close>    
  definition "wlp c Q s \<equiv> \<forall>t. \<pi>:(c,s) \<Rightarrow> t \<longrightarrow> Q t"
  
  subsubsection \<open>Basic Properties\<close>
  
  context 
    notes [abs_def,simp] = wp_def wlp_def
  begin
    lemma wp_imp_wlp: "wp c Q s \<Longrightarrow> wlp c Q s"
      using big_step_determ by force
      
    lemma wlp_and_term_imp_wp: "wlp c Q s \<and> \<pi>:(c,s) \<Rightarrow> t \<Longrightarrow> wp c Q s" by auto    
    
    lemma wp_equiv: "c \<sim> c' \<Longrightarrow> wp c = wp c'" by auto
    lemma wp_conseq: "wp c P s \<Longrightarrow> \<lbrakk>\<And>s. P s \<Longrightarrow> Q s\<rbrakk> \<Longrightarrow> wp c Q s" by auto
    
    lemma wlp_equiv: "c \<sim> c' \<Longrightarrow> wlp c = wlp c'" by auto
    lemma wlp_conseq: "wlp c P s \<Longrightarrow> \<lbrakk>\<And>s. P s \<Longrightarrow> Q s\<rbrakk> \<Longrightarrow> wlp c Q s" by auto
    
      
  
  subsubsection \<open>Unfold Rules\<close>
    lemma wp_skip_eq: "wp SKIP Q s = Q s" by auto
    lemma wp_assign_idx_eq: "wp (x[i]::=a) Q s = Q (s(x:=(s x)(aval i s := aval a s)))" by auto
    lemma wp_arraycpy_eq: "wp (x[]::=a) Q s = Q (s(x:=s a))" by auto
    lemma wp_arrayinit_eq: "wp (CLEAR x[]) Q s = Q (s(x:=(\<lambda>_. 0)))" by auto
    lemma wp_seq_eq: "wp (c\<^sub>1;;c\<^sub>2) Q s = wp c\<^sub>1 (wp c\<^sub>2 Q) s" by auto
    lemma wp_if_eq: "wp (IF b THEN c\<^sub>1 ELSE c\<^sub>2) Q s 
      = (if bval b s then wp c\<^sub>1 Q s else wp c\<^sub>2 Q s)" by auto
    
    lemma wp_scope_eq: "wp (SCOPE c) Q s = wp c (\<lambda>s'. Q <s|s'>) <<>|s>" by auto
    lemma wp_pcall_eq: "\<pi> p = Some c \<Longrightarrow> wp (PCall p) Q s = wp c Q s" by auto 
    
    lemmas wp_eq = wp_skip_eq wp_assign_idx_eq wp_arraycpy_eq wp_arrayinit_eq wp_seq_eq wp_scope_eq
    lemmas wp_eq' = wp_eq wp_if_eq
    
    lemma wlp_skip_eq: "wlp SKIP Q s = Q s" by auto
    lemma wlp_assign_idx_eq: "wlp (x[i]::=a) Q s = Q (s(x:=(s x)(aval i s := aval a s)))" by auto
    lemma wlp_arraycpy_eq: "wlp (x[]::=a) Q s = Q (s(x:=s a))" by auto
    lemma wlp_arrayinit_eq: "wlp (CLEAR x[]) Q s = Q (s(x:=(\<lambda>_. 0)))" by auto
    lemma wlp_seq_eq: "wlp (c\<^sub>1;;c\<^sub>2) Q s = wlp c\<^sub>1 (wlp c\<^sub>2 Q) s" by auto
    lemma wlp_if_eq: "wlp (IF b THEN c\<^sub>1 ELSE c\<^sub>2) Q s 
      = (if bval b s then wlp c\<^sub>1 Q s else wlp c\<^sub>2 Q s)" by auto

    lemma wlp_scope_eq: "wlp (SCOPE c) Q s = wlp c (\<lambda>s'. Q <s|s'>) <<>|s>" by auto
    lemma wlp_pcall_eq: "\<pi> p = Some c \<Longrightarrow> wlp (PCall p) Q s = wlp c Q s" by auto 
      
          
    lemmas wlp_eq = wlp_skip_eq wlp_assign_idx_eq wlp_arraycpy_eq wlp_arrayinit_eq wlp_seq_eq wlp_scope_eq
    lemmas wlp_eq' = wlp_eq wlp_if_eq
  end
  
  lemma wlp_while_unfold: "wlp (WHILE b DO c) Q s = (if bval b s then wlp c (wlp (WHILE b DO c) Q) s else Q s)"
    apply (subst wlp_equiv[OF while_unfold])
    apply (simp add: wlp_eq')
    done
  
  lemma wp_while_unfold: "wp (WHILE b DO c) Q s = (if bval b s then wp c (wp (WHILE b DO c) Q) s else Q s)"
    apply (subst wp_equiv[OF while_unfold])
    apply (simp add: wp_eq')
    done
    
end \<comment> \<open>Context fixing program\<close>

lemma wp_pscope_eq: "wp \<pi> (PScope \<pi>' p) Q s = wp \<pi>' (PCall p) Q s"
  unfolding wp_def by auto

lemma wlp_pscope_eq: "wlp \<pi> (PScope \<pi>' p) Q s = wlp \<pi>' (PCall p) Q s"
  unfolding wlp_def by auto
  

lemma wp_equiv_iff: "(\<forall>\<pi>. wp \<pi> c = wp \<pi> c') \<longleftrightarrow> c \<sim> c'"
  unfolding equiv_c_def
  using big_step_determ unfolding wp_def
  by (auto; metis)

lemma wlp_equiv_iff: "(\<forall>\<pi>. wlp \<pi> c = wlp \<pi> c') \<longleftrightarrow> c \<sim> c'" 
  unfolding equiv_c_def wlp_def
  by (auto; metis (no_types, hide_lams))


lemma sim_while_wp_aux:
  assumes "bval b = bval b'" 
  assumes "wp \<pi> c = wp \<pi> c'" 
  assumes "\<pi>: (WHILE b DO c, s) \<Rightarrow> t"
  shows "\<pi>: (WHILE b' DO c', s) \<Rightarrow> t"
  using assms(3,2)
  apply (induction \<pi> "WHILE b DO c" s t)
  apply (auto simp: assms(1))
  by (metis WhileTrue big_step_determ wp_def)
        
lemma sim_while_wp: "bval b = bval b' \<Longrightarrow> wp \<pi> c = wp \<pi> c' \<Longrightarrow> wp \<pi> (WHILE b DO c) = wp \<pi> (WHILE b' DO c')"
  apply (intro ext)
  apply (auto 0 3 simp: wp_def intro: sim_while_wp_aux)
  done

lemma sim_while_wlp_aux:
  assumes "bval b = bval b'" 
  assumes "wlp \<pi> c = wlp \<pi> c'" 
  assumes "\<pi>: (WHILE b DO c, s) \<Rightarrow> t"
  shows "\<pi>: (WHILE b' DO c', s) \<Rightarrow> t"
  using assms(3,2)
  apply (induction \<pi> "WHILE b DO c" s t)
  apply (auto simp: assms(1,2))
  by (metis WhileTrue wlp_def)
        
lemma sim_while_wlp: "bval b = bval b' \<Longrightarrow> wlp \<pi> c = wlp \<pi> c' \<Longrightarrow> wlp \<pi> (WHILE b DO c) = wlp \<pi> (WHILE b' DO c')"
  apply (intro ext)
  apply (auto 0 3 simp: wlp_def intro: sim_while_wlp_aux)
  done
  
      
subsection \<open>Invariants for While-Loops\<close>
    
  subsubsection \<open>Partial Correctness\<close>
  lemma wlp_whileI':
    assumes INIT: "I s\<^sub>0"
    assumes STEP: "\<And>s. I s \<Longrightarrow> (if bval b s then wlp \<pi> c I s else Q s)"
    shows "wlp \<pi> (WHILE b DO c) Q s\<^sub>0"
    unfolding wlp_def
  proof clarify
    fix t
    assume "\<pi>: (WHILE b DO c, s\<^sub>0) \<Rightarrow> t"
    thus "Q t" using INIT STEP
    proof (induction \<pi> "WHILE b DO c" s\<^sub>0 t rule: big_step_induct)
      case (WhileFalse s) with STEP show "Q s" by auto
    next
      case (WhileTrue s\<^sub>1 \<pi> s\<^sub>2 s\<^sub>3)
      note STEP' = WhileTrue.prems(2)
      
      from STEP'[OF \<open>I s\<^sub>1\<close>] \<open>bval b s\<^sub>1\<close> have "wlp \<pi> c I s\<^sub>1" by simp
      with \<open>\<pi>: (c, s\<^sub>1) \<Rightarrow> s\<^sub>2\<close> have "I s\<^sub>2" unfolding wlp_def by blast
      moreover have \<open>I s\<^sub>2 \<Longrightarrow> Q s\<^sub>3\<close> using STEP' WhileTrue.hyps(5) by blast 
      ultimately show "Q s\<^sub>3" by blast
    qed
  qed

  (* Short proof (not the shortest possible one ;) ) *)
  lemma 
    assumes INIT: "I s\<^sub>0"
    assumes STEP: "\<And>s. I s \<Longrightarrow> (if bval b s then wlp \<pi> c I s else Q s)"
    shows "wlp \<pi> (WHILE b DO c) Q s\<^sub>0"
    using STEP
    unfolding wlp_def
    apply clarify subgoal premises prems for t
      using prems(2,1) INIT
      by (induction \<pi> "WHILE b DO c" s\<^sub>0 t rule: big_step_induct; meson)
    done
    
  subsubsection \<open>Total Correctness\<close>
  
  lemma wp_whileI':
    assumes WF: "wf R"
    assumes INIT: "I s\<^sub>0"
    assumes STEP: "\<And>s. I s \<Longrightarrow> (if bval b s then wp \<pi> c (\<lambda>s'. I s' \<and> (s',s)\<in>R) s else Q s)"
    shows "wp \<pi> (WHILE b DO c) Q s\<^sub>0"
    using WF INIT 
  proof (induction rule: wf_induct_rule[where a=s\<^sub>0]) (* Instantiation required to avoid strange HO-unification problem! *)
    case (less s)
    show "wp \<pi> (WHILE b DO c) Q s" 
    proof (rule wp_while_unfold[THEN iffD2])
      show "if bval b s then wp \<pi> c (wp \<pi> (WHILE b DO c) Q) s else Q s" 
      proof (split if_split; intro allI impI conjI)
        assume [simp]: "bval b s"
        
        from STEP \<open>I s\<close> have "wp \<pi> c (\<lambda>s'. I s' \<and> (s',s)\<in>R) s" by simp
        thus "wp \<pi> c (wp \<pi> (WHILE b DO c) Q) s" proof (rule wp_conseq)
          fix s' assume "I s' \<and> (s',s)\<in>R"
          with less.IH show "wp \<pi> (WHILE b DO c) Q s'" by blast
        qed
      next
        assume [simp]: "\<not>bval b s"
        from STEP \<open>I s\<close> show "Q s" by simp
      qed
    qed
  qed

  (* Short Proof *)  
  lemma 
    assumes WF: "wf R"
    assumes INIT: "I s\<^sub>0"
    assumes STEP: "\<And>s. I s \<Longrightarrow> (if bval b s then wp \<pi> c (\<lambda>s'. I s' \<and> (s',s)\<in>R) s else Q s)"
    shows "wp \<pi> (WHILE b DO c) Q s\<^sub>0"
    using WF INIT 
    apply (induction rule: wf_induct_rule[where a=s\<^sub>0])
    apply (subst wp_while_unfold)
    by (smt STEP wp_conseq)
    
    
  subsubsection \<open>Standard Forms of While Rules\<close>  
  lemma wlp_whileI:
    assumes INIT: "I \<ss>\<^sub>0"
    assumes STEP: "\<And>\<ss>. \<lbrakk>I \<ss>; bval b \<ss> \<rbrakk> \<Longrightarrow> wlp \<pi> c I \<ss>"
    assumes FINAL: "\<And>\<ss>. \<lbrakk> I \<ss>; \<not>bval b \<ss> \<rbrakk> \<Longrightarrow> Q \<ss>"
    shows "wlp \<pi> (WHILE b DO c) Q \<ss>\<^sub>0"
    using assms wlp_whileI' by auto
    
    
  lemma wp_whileI:
    assumes WF: "wf R"
    assumes INIT: "I \<ss>\<^sub>0"
    assumes STEP: "\<And>\<ss>. \<lbrakk>I \<ss>; bval b \<ss> \<rbrakk> \<Longrightarrow> wp \<pi> c (\<lambda>\<ss>'. I \<ss>' \<and> (\<ss>',\<ss>)\<in>R) \<ss>"
    assumes FINAL: "\<And>\<ss>. \<lbrakk> I \<ss>; \<not>bval b \<ss> \<rbrakk> \<Longrightarrow> Q \<ss>"
    shows "wp \<pi> (WHILE b DO c) Q \<ss>\<^sub>0"
    using assms wp_whileI' by auto


subsection \<open>Modularity of Programs\<close>    

lemma map_leD: "m\<subseteq>\<^sub>mm' \<Longrightarrow> m x = Some v \<Longrightarrow> m' x = Some v"
  by (metis domI map_le_def)
  
lemma big_step_mono_prog:
  assumes "\<pi> \<subseteq>\<^sub>m \<pi>'"  
  assumes "\<pi>:(c,s) \<Rightarrow> t"
  shows "\<pi>':(c,s) \<Rightarrow> t"
  using assms(2,1)
  apply (induction \<pi> c s t rule: big_step_induct)
  by (auto dest: map_leD)
    
  
text \<open>Wrap a set of recursive procedures into a procedure scope\<close>  
lemma localize_recursion:
  "\<pi>': (PScope \<pi> p, s) \<Rightarrow> t \<longleftrightarrow> \<pi>:(PCall p,s) \<Rightarrow> t"
  by auto
  
  
  
subsection \<open>Strongest Postcondition\<close>  

context fixes \<pi> :: program begin
  definition "sp P c t \<equiv> \<exists>s. P s \<and> \<pi>: (c,s) \<Rightarrow> t"

  context notes [simp] = sp_def[abs_def] begin
  
    text \<open>Intuition: There exists an old value \<open>vx\<close> for the assigned variable\<close>
    lemma sp_arraycpy_eq: "sp P (x[]::=y) t \<longleftrightarrow> (\<exists>vx. let s = t(x:=vx) in t x = s y \<and> P s)" 
      apply (auto simp: big_step_simps)
      apply (intro exI conjI, assumption, auto) []
      apply (intro exI conjI, assumption, auto) []
      done
    
    text \<open>Version with renaming of assigned variable\<close>
    lemma sp_arraycpy_eq': "sp P (x[]::=y) t \<longleftrightarrow> t x = t y \<and> (\<exists>vx. P (t(x:=vx,y:=t x)))" 
      apply (auto simp: big_step_simps)
      apply (metis fun_upd_triv)
      apply (intro exI conjI, assumption) apply auto
      done
      
      
    lemma sp_skip_eq: "sp P SKIP t \<longleftrightarrow> P t" by auto
    lemma sp_seq_eq: "sp P (c\<^sub>1;;c\<^sub>2) t \<longleftrightarrow> sp (sp P c\<^sub>1) c\<^sub>2 t" by auto
    
  end  
end
  
subsection \<open>Hoare-Triples\<close>    
        
definition HT 
  where "HT \<pi> P c Q \<equiv> (\<forall>s\<^sub>0. P s\<^sub>0 \<longrightarrow> wp \<pi> c (Q s\<^sub>0) s\<^sub>0)"

definition HT_partial
  where "HT_partial \<pi> P c Q \<equiv> (\<forall>s\<^sub>0. P s\<^sub>0 \<longrightarrow> wlp \<pi> c (Q s\<^sub>0) s\<^sub>0)"

lemma HT_conseq: 
  assumes "HT \<pi> P c Q"
  assumes "\<And>s. P' s \<Longrightarrow> P s"
  assumes "\<And>s\<^sub>0 s. \<lbrakk>P s\<^sub>0; P' s\<^sub>0; Q s\<^sub>0 s \<rbrakk> \<Longrightarrow> Q' s\<^sub>0 s"
  shows "HT \<pi> P' c Q'"
  using assms unfolding HT_def by (blast intro: wp_conseq)

lemma HT_partial_conseq: 
  assumes "HT_partial \<pi> P c Q"
  assumes "\<And>s. P' s \<Longrightarrow> P s"
  assumes "\<And>s\<^sub>0 s. \<lbrakk>P s\<^sub>0; P' s\<^sub>0; Q s\<^sub>0 s \<rbrakk> \<Longrightarrow> Q' s\<^sub>0 s"
  shows "HT_partial \<pi> P' c Q'"
  using assms unfolding HT_partial_def by (blast intro: wlp_conseq)
  
  
text \<open>Simple rule for presentation in lecture: Reuse a Hoare-triple during VCG\<close>
lemma wp_modularity_rule:
  "\<lbrakk>HT \<pi> P c Q; P s; (\<And>s'. Q s s' \<Longrightarrow> Q' s')\<rbrakk> \<Longrightarrow> wp \<pi> c Q' s"
  unfolding HT_def
  by (blast intro: wp_conseq)
  
  
  
type_synonym htset = "((state \<Rightarrow> bool) \<times> com \<times> (state \<Rightarrow> state \<Rightarrow> bool)) set"

definition "HTset \<pi> \<Theta> \<equiv> \<forall>(P,c,Q)\<in>\<Theta>. HT \<pi> P c Q"
    
definition "HTset_r r \<pi> \<Theta> \<equiv> \<forall>(P,c,Q)\<in>\<Theta>. HT \<pi> (\<lambda>s. r c s \<and> P s) c Q"
    
subsubsection \<open>Deriving Parameter Frame Adjustment Rules\<close>

text \<open> Intuition: 
  New precondition is weakest one we need to ensure \<open>P\<close> after prologue
\<close>
lemma adjust_prologue:
  assumes "HT \<pi> P body Q"
  shows "HT \<pi> (wp \<pi> prologue P) (prologue;;body) (\<lambda>s\<^sub>0 s. wp \<pi> prologue (\<lambda>s\<^sub>0. Q s\<^sub>0 s) s\<^sub>0)"
  using assms
  unfolding HT_def
  apply (auto simp: wp_eq)
  using wp_def by fastforce

text \<open> Intuition:
  New postcondition is strongest one we can get from \<open>Q\<close> after epilogue.
  
  We have to be careful with non-terminating epilogue, though!
\<close>  
lemma adjust_epilogue:
  assumes "HT \<pi> P body Q"  
  assumes TERMINATES: "\<forall>s. \<exists>t. \<pi>: (epilogue,s) \<Rightarrow> t"
  shows "HT \<pi> P (body;;epilogue) (\<lambda>s\<^sub>0. sp \<pi> (Q s\<^sub>0) epilogue)"
  using assms
  unfolding HT_def
  apply (simp add: wp_eq)
  apply (force simp: sp_def wp_def)
  done
  
text \<open>Intuition: 
  Scope can be seen as assignment of locals before and after inner command.
  Thus, this rule is a combined forward and backward assignment rule, for
  the epilogue \<open>locals:=<>\<close> and the prologue \<open>locals:=old_locals\<close>.
\<close>  
lemma adjust_scope:
  assumes "HT \<pi> P body Q"
  shows "HT \<pi> (\<lambda>s. P <<>|s>) (SCOPE body) (\<lambda>s\<^sub>0 s. \<exists>l. Q (<<>|s\<^sub>0>) (<l|s>))"
  using assms unfolding HT_def
  apply (auto simp: wp_eq combine_nest)
  apply (auto simp: wp_def) 
  by (metis combine_collapse)

subsubsection \<open>Proof for Recursive Specifications\<close>

text \<open>Prove correct any set of Hoare-triples, e.g., mutually recursive ones\<close>
lemma HTsetI:    
  assumes "wf R"
  assumes RL: "\<And>P c Q s\<^sub>0. \<lbrakk> HTset_r (\<lambda>c' s'. ((c',s'),(c,s\<^sub>0))\<in>R ) \<pi> \<Theta>; (P,c,Q)\<in>\<Theta>; P s\<^sub>0 \<rbrakk> \<Longrightarrow> wp \<pi> c (Q s\<^sub>0) s\<^sub>0"
  shows "HTset \<pi> \<Theta>"
  unfolding HTset_def HT_def 
proof clarsimp
  fix P c Q s\<^sub>0
  assume "(P,c,Q)\<in>\<Theta>" "P s\<^sub>0"
  with \<open>wf R\<close> show "wp \<pi> c (Q s\<^sub>0) s\<^sub>0"
    apply (induction "(c,s\<^sub>0)" arbitrary: c s\<^sub>0 P Q)
    using RL unfolding HTset_r_def HT_def
    by blast
    
qed  
    

lemma HT_simple_recursiveI:
  assumes "wf R"
  assumes "\<And>s. \<lbrakk>HT \<pi> (\<lambda>s'. (f s', f s)\<in>R \<and> P s') c Q; P s \<rbrakk> \<Longrightarrow> wp \<pi> c (Q s) s"
  shows "HT \<pi> P c Q"
  using HTsetI[where R="inv_image R (f o snd)" and \<pi>=\<pi> and \<Theta> = "{(P,c,Q)}"] assms
  by (auto simp: HTset_r_def HTset_def)


lemma HT_simple_recursive_procI:
  assumes "wf R"
  assumes "\<And>s. \<lbrakk>HT \<pi> (\<lambda>s'. (f s', f s)\<in>R \<and> P s') (PCall p) Q; P s \<rbrakk> \<Longrightarrow> wp \<pi> (PCall p) (Q s) s"
  shows "HT \<pi> P (PCall p) Q"
  using HTsetI[where R="inv_image R (f o snd)" and \<pi>=\<pi> and \<Theta> = "{(P,PCall p,Q)}"] assms
  by (auto simp: HTset_r_def HTset_def)
  
  
    
lemma
  assumes "wf R"
  assumes "\<And>s P p Q. \<lbrakk> 
    \<And>P' p' Q'. (P',p',Q')\<in>\<Theta> 
      \<Longrightarrow> HT \<pi> (\<lambda>s'. ((p',s'),(p,s))\<in>R \<and> P' s') (PCall p') Q';
    (P,p,Q)\<in>\<Theta>; P s 
  \<rbrakk> \<Longrightarrow> wp \<pi> (PCall p) (Q s) s"
  shows "\<forall>(P,p,Q)\<in>\<Theta>. HT \<pi> P (PCall p) Q"  
proof -

  have "HTset \<pi> {(P, PCall p, Q) |P p Q. (P, p, Q) \<in> \<Theta>}"
    apply (rule HTsetI[where R="inv_image R (\<lambda>x. case x of (PCall p,s) \<Rightarrow> (p,s))"])
    subgoal using \<open>wf R\<close> by simp
    subgoal for P c Q s
      apply clarsimp
      apply (rule assms(2)[where P=P])
      apply simp_all
      unfolding HTset_r_def
      proof goal_cases
        case (1 p P' p' Q')
        
        from "1"(1)[rule_format, of "(P',PCall p',Q')", simplified] "1"(2-)
          show ?case by auto
      qed
    done
    
  thus ?thesis by (auto simp: HTset_def)
qed
    

subsection \<open>Completeness of While-Rule\<close>

text \<open>Idea: Use \<open>wlp\<close> as invariant\<close>
lemma wlp_whileI'_complete:
  assumes "wlp \<pi> (WHILE b DO c) Q s\<^sub>0"
  obtains I where
    "I s\<^sub>0"
    "\<And>s. I s \<Longrightarrow> if bval b s then wlp \<pi> c I s else Q s"
proof
  let ?I = "wlp \<pi> (WHILE b DO c) Q"
  {
    show "?I s\<^sub>0" by fact
  next
    fix s
    assume "?I s"
    then show "if bval b s then wlp \<pi> c ?I s else Q s"
      apply (subst (asm) wlp_while_unfold) 
      .
  }  
qed
   
text \<open>Idea: Remaining loop iterations as variant\<close>

inductive count_it for \<pi> b c where
  "\<not>bval b s \<Longrightarrow> count_it \<pi> b c s 0"
| "\<lbrakk>bval b s; \<pi>: (c,s) \<Rightarrow> s'; count_it \<pi> b c s' n \<rbrakk> \<Longrightarrow> count_it \<pi> b c s (Suc n )"  

lemma count_it_determ:
  "count_it \<pi> b c s n \<Longrightarrow> count_it \<pi> b c s n' \<Longrightarrow> n' = n"
  apply (induction arbitrary: n' rule: count_it.induct)
  subgoal using count_it.cases by blast 
  subgoal by (metis big_step_determ count_it.cases)
  done

lemma count_it_ex:   
  assumes "\<pi>: (WHILE b DO c,s) \<Rightarrow> t"
  shows "\<exists>n. count_it \<pi> b c s n"
  using assms
  apply (induction \<pi> "WHILE b DO c" s t arbitrary: b c)
  apply (auto intro: count_it.intros)
  done

definition "variant \<pi> b c s \<equiv> THE n. count_it \<pi> b c s n"  

lemma variant_decreases:
  assumes STEPB: "bval b s" 
  assumes STEPC: "\<pi>: (c,s) \<Rightarrow> s'" 
  assumes TERM: "\<pi>: (WHILE b DO c,s') \<Rightarrow> t"
  shows "variant \<pi> b c s' < variant \<pi> b c s"
proof -
  from count_it_ex[OF TERM] obtain n' where CI': "count_it \<pi> b c s' n'" ..
  moreover from count_it.intros(2)[OF STEPB STEPC this] have "count_it \<pi> b c s (Suc n')" .
  ultimately have "variant \<pi> b c s' = n'" "variant \<pi> b c s = Suc n'" 
    unfolding variant_def using count_it_determ by blast+
  thus ?thesis by simp 
qed

lemma wp_whileI'_complete:
  fixes \<pi> b c
  defines "R\<equiv>measure (variant \<pi> b c)"
  assumes "wp \<pi> (WHILE b DO c) Q s\<^sub>0"
  obtains I where
    "wf R"
    "I s\<^sub>0"
    "\<And>s. I s \<Longrightarrow> if bval b s then wp \<pi> c (\<lambda>s'. I s' \<and> (s',s)\<in>R) s else Q s"
proof   
  show \<open>wf R\<close> unfolding R_def by auto
  let ?I = "wp \<pi> (WHILE b DO c) Q"
  {
    show "?I s\<^sub>0" by fact
  next
    fix s
    assume "?I s"
    then show "if bval b s then wp \<pi> c (\<lambda>s'. ?I s' \<and> (s',s)\<in>R) s else Q s"
      apply (subst (asm) wp_while_unfold) 
      apply clarsimp
      by (auto simp: wp_def R_def intro: variant_decreases)
      
  }  
qed  
    
    
      
  
  
end
                                 