section \<open>Syntax of IMP2\<close>
theory Syntax
imports Main
begin

  text \<open>We define the abstract syntax of the IMP2 language, 
    and a minimal concrete syntax for direct use in terms.\<close>


  type_synonym vname = string
  type_synonym pname = string
  type_synonym val = "int \<Rightarrow> int"
  type_synonym pval = "int"
  
  fun is_global :: "vname \<Rightarrow> bool" where
    "is_global [] \<longleftrightarrow> True"
  | "is_global (CHR ''G''#_) \<longleftrightarrow> True"
  | "is_global _ \<longleftrightarrow> False"

  abbreviation "is_local a \<equiv> \<not>is_global a"
  
  (*
    TODO: Cf. SortedAlgebra, used in CeTa. Akihisa Yamada
      Should give aexp, bexp with type-checking, term ops, etc.
  
  *)
  
  datatype aexp = 
      N int 
    | Vidx vname aexp
    | Unop "int \<Rightarrow> int" aexp 
    | Binop "int \<Rightarrow> int \<Rightarrow> int" aexp aexp
    
  abbreviation "V x \<equiv> Vidx x (N 0)"  
    
  datatype bexp = 
      Bc bool 
    | Not bexp 
    | BBinop "bool \<Rightarrow> bool \<Rightarrow> bool" bexp bexp 
    | Cmpop "int \<Rightarrow> int \<Rightarrow> bool" aexp aexp

  datatype
    com = SKIP 
        | AssignIdx vname aexp aexp       ("_[_] ::= _" [1000, 0, 61] 61)
        | ArrayCpy vname vname    ("_[] ::= _" [1000, 1000] 61)
        | ArrayClear vname        ("CLEAR _[]" [1000] 61)
        | Seq    com  com         ("_;;/ _"  [61, 60] 60)
        | If     bexp com com     ("(IF _/ THEN _/ ELSE _)"  [0, 0, 61] 61)
        | While  bexp com         ("(WHILE _/ DO _)"  [0, 61] 61)
        | Scope com               ("SCOPE _" [61] 61)
        | PCall pname             
        | PScope "pname \<rightharpoonup> com" pname

        
  type_synonym program = "pname \<rightharpoonup> com"
        
        
        
  abbreviation Assign ("_ ::= _" [1000, 61] 61) where "x ::= a \<equiv> (x[N 0] ::= a)"
        
        
   
end
