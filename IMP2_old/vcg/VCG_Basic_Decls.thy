theory VCG_Basic_Decls
imports VCG_Basic_Simpset
begin

  named_theorems analysis_unfolds \<open>Unfold theorems to be applied prior to program analysis functions\<close>
  
  
  named_theorems vcg_preprocess_rules \<open>Rules to be applied to goal before VCG\<close>

  
  named_theorems vcg_specs \<open>Specifications declared to VCG\<close>
    

end
