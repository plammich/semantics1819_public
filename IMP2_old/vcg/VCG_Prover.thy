(* TODO: Rename to VCG_Prover *)
theory VCG_Prover
imports VCG_Basic_Simpset VCG_Program_Analysis VCG_Var_Postprocessor VCG_Var_Abs
begin


section \<open>Generic Verification Condition Generator\<close>

  (*
  declare [[ML_debugger,ML_exception_trace,ML_exception_debugger]]
  *)


  subsection \<open>Extending Basic Simpset\<close>
  
  definition UPD_STATE :: "state \<Rightarrow> vname \<Rightarrow> val \<Rightarrow> state" where
    "UPD_STATE s x v \<equiv> s(x:=v)"

  definition UPD_IDX :: "val \<Rightarrow> int \<Rightarrow> pval \<Rightarrow> val" where
    "UPD_IDX av i pv \<equiv> av(i:=pv)"
        
  definition UPD_STATE_IDX :: "state \<Rightarrow> vname \<Rightarrow> int \<Rightarrow> pval \<Rightarrow> state" where
    "UPD_STATE_IDX s x i v \<equiv> s(x:=UPD_IDX (s x) i v)"
    

  lemma [named_ss vcg_bb]:
    "UPD_STATE (s(x:=w)) x v = s(x:=v)"
    "x\<noteq>y \<Longrightarrow> UPD_STATE (s(x:=w)) y v = (UPD_STATE s y v)(x:=w)"  
    "NO_MATCH (SS(XX:=VV)) s \<Longrightarrow> UPD_STATE s x v = s(x:=v)"
    by (auto simp: UPD_STATE_def)
        
  lemma [named_ss vcg_bb]:
    "UPD_STATE_IDX (s(x:=w)) x i v = s(x:=UPD_IDX w i v)"
    "x\<noteq>y \<Longrightarrow> UPD_STATE_IDX (s(x:=w)) y i v = (UPD_STATE_IDX s y i v)(x:=w)"  
    "NO_MATCH (SS(XX:=VV)) s \<Longrightarrow> UPD_STATE_IDX s x i v = s(x:=UPD_IDX (s x) i v)"
    by (auto simp: UPD_STATE_IDX_def)
 
  (* Note (hack): These two rewrite rules exploit inner-to-outer rewrite order
    to ensure that the first rule is applied first.
  *)  
  lemma [named_ss vcg_bb]:  
    "UPD_IDX (a(i:=v)) i = UPD_IDX a i"
    "UPD_IDX a i v = (a(i:=v))"          
    by (auto simp: UPD_IDX_def)
    

  
  lemma modifies_equals': "ASSUMPTION (modifies vs s s') \<Longrightarrow> x\<notin>vs \<Longrightarrow> s x = s' x" 
    unfolding ASSUMPTION_def by (simp add: modifies_equals)
  
  lemmas [named_ss vcg_bb] = modifies_equals' modifies_upd
  
  
  notepad begin
    have "ANALYZE (lhsv Map.empty \<^imp>\<open>scope { x=5; y=5; GX=6; GY=6; z=7 }\<close>) = {''GY'',''GX''}"
      by (simp named_ss vcg_bb: )

    have "ANALYZE (lhsv [''foo'' \<mapsto> \<^imp>\<open>a=42; GZ=7\<close>] \<^imp>\<open>scope { x=5; y=5; GX=6; GY=6; z=7; rec foo() }\<close>) = {''GX'', ''GZ'', ''GY''}"
      by (simp named_ss vcg_bb: )
      
            
    have "fv_aexp (Vidx ''x'' (Unop uminus (V ''y''))) = {''x'',''y''}"  
      by (simp named_ss vcg_bb: )
      
  end

  (* Transfer naming hints over substitutions *)
  lemma NAMING_HINT_SUBST[named_ss vcg_bb]: "NAMING_HINT (s(x:=y)) v n = NAMING_HINT s v n" by (simp add: NAMING_HINT_def) 
  lemma VAR_same[named_ss vcg_bb]: "VAR a (\<lambda>x. VAR a (\<lambda>y. P x y)) = VAR a (\<lambda>x. P x x)"
    by (auto simp: VAR_def)
  
  subsection \<open>Preprocessing\<close>
  
  lemma HT'_I[vcg_preprocess_rules]:
    assumes "\<And>\<ss>\<^sub>0. \<lbrakk> BB_PROTECT P \<ss>\<^sub>0 \<rbrakk> \<Longrightarrow> wp \<pi> (c \<ss>\<^sub>0) (BB_PROTECT Q \<ss>\<^sub>0) \<ss>\<^sub>0"
    shows "HT' \<pi> P c Q"
    using assms
    unfolding BB_PROTECT_def HT'_def by auto
  
  lemma HT'_partial_I[vcg_preprocess_rules]:
    assumes "\<And>\<ss>\<^sub>0. \<lbrakk> BB_PROTECT P \<ss>\<^sub>0 \<rbrakk> \<Longrightarrow> wlp \<pi> (c \<ss>\<^sub>0) (BB_PROTECT Q \<ss>\<^sub>0) \<ss>\<^sub>0"
    shows "HT'_partial \<pi> P c Q"
    using assms
    unfolding BB_PROTECT_def HT'_partial_def by auto
  
  lemma protect_annotations: "ANNOTATION P \<equiv> BB_PROTECT P" 
    by (simp add: BB_PROTECT_def ANNOTATION_def) 
    
  lemmas [vcg_preprocess_rules] = allI impI conjI
    
  method i_vcg_preprocess = 
    (simp only: protect_annotations cong: BB_PROTECT_cong)?; 
    (intro vcg_preprocess_rules)?

    
  subsection \<open>Approximation of Semantics\<close>
  text \<open>
    The next step of the VCG is to process the semantic constants to insert approximations, 
    as induced, e.g., by invariants or re-using Hoare-triples for procedures.
    
    This phase will first expose the variable bindings of user annotations. 
    Then, it applies a set of introduction rules \<open>vcg-rules\<close>, and finally 
    simplifies with \<open>vcg_bb\<close> to do variable name computations, e.g., due to modifies-clauses
    introduced by while-rules, or due to scope-combinations.
  \<close>
  (*
    TODO: Currently, this phase already splits the goal. 
    However, it would be cleaner to just apply monotonicity rules at this point,
    to obtain a single goal again, that does not explode!

    This will need a rewriting wrt.~monotonicity tool, which we do not have in Isabelle (yet).
  *)
  
  named_theorems vcg_rules \<open>Rules for VCG Approximation Phase\<close>

  text \<open>Frame rules are only be applied if their first assumption can be 
    discharged by assumption or a vcg_rule.\<close>
  named_theorems vcg_frame_rules \<open>Frame rules for VCG Approximation Phase\<close>
  
  
        
  method i_vcg_rule = (rule vcg_rules | rule vcg_frame_rules, ((assumption|rule vcg_specs);fail))
  
  method i_vcg_explode_approx = (i_vcg_rule; i_vcg_bb?; i_vcg_explode_approx | i_vcg_bb?)
  (* TODO: It might make sense to do some (more) in-processing here *)  
  
  method i_vcg_approx =     
    (unfold unprotect_VAR)?;
    (i_vcg_explode_approx)?;
    i_vcg_bb?
    
    

  subsection \<open>Postprocessing of VCs\<close>
  
  method i_vcg_remove_hints = ((determ \<open>thin_tac "modifies _ _ _"\<close>)+)?; i_vcg_remove_renamings_tac
    
  
  text \<open>Postprocessing exposes user annotations completely, and then abstracts over variable names.\<close>
  method i_vcg_postprocess =
    i_vcg_apply_renamings_tac?;
    i_vcg_remove_hints?;
    (unfold BB_PROTECT_def VAR_def)?;
    i_vcg_postprocess_vars
        
    
  subsection \<open>VCG Main Interface\<close>  
                                                                    
  method vcg declares vcg_rules = i_vcg_preprocess; i_vcg_bb?; i_vcg_approx; i_vcg_postprocess
    \<comment> \<open>Preprocess, generate VCs, and postprocess them\<close>
    
  method vcg_cs declares vcg_rules = vcg; clarsimp?
    \<comment> \<open>Generate VCs, postprocess and clarsimp them\<close>
  
  method vcg_auto declares vcg_rules = vcg; (auto;fail | clarsimp?)
    \<comment> \<open>Recursively apply all possible steps, try hard to solve VCs\<close>

  method vcg_force declares vcg_rules = vcg; (force | clarsimp?)
    \<comment> \<open>Recursively apply all possible steps, try even harder to solve VCs\<close>
    
    
    
section \<open>VCG Setup\<close>
    
  subsection \<open>Unfolding Phase\<close>    
  text \<open>Aval and Bval\<close>  
  lemmas [named_ss vcg_bb] = aval.simps bval.simps
    
  text \<open>Unfolding basic block commands\<close>
  
  lemma vcg_assign_idx_unfolds[named_ss vcg_bb]:
    "wlp \<pi> (x[i] ::= a) Q s = Q (UPD_STATE_IDX s x (aval i s) (aval a s))"
    "wp \<pi> (x[i] ::= a) Q s = Q (UPD_STATE_IDX s x (aval i s) (aval a s))"
    unfolding UPD_STATE_IDX_def UPD_IDX_def 
    by (simp_all add: wlp_eq wp_eq)
  
  lemma vcg_arraycpy_unfolds[named_ss vcg_bb]:
    "wlp \<pi> (x[] ::= a) Q s = Q (UPD_STATE s x (s a))"
    "wp \<pi> (x[] ::= a) Q s = Q (UPD_STATE s x (s a))"
    unfolding UPD_STATE_def 
    by (simp_all add: wlp_eq wp_eq)

  lemma vcg_arrayinit_unfolds[named_ss vcg_bb]:
    "wlp \<pi> (CLEAR x[]) Q s = Q (UPD_STATE s x (\<lambda>_. 0))"
    "wp \<pi> (CLEAR x[]) Q s = Q (UPD_STATE s x (\<lambda>_. 0))"
    unfolding UPD_STATE_def 
    by (simp_all add: wlp_eq wp_eq)
    
  text \<open>Special case for procedure return value: 
    Insert a renaming to keep name of original variable\<close>  

  definition INSERT_NAMING_HINT :: "string \<Rightarrow> string \<Rightarrow> (state \<Rightarrow> bool) \<Rightarrow> state \<Rightarrow> bool"
    where "INSERT_NAMING_HINT x n W s \<equiv> W s"  
  
  lemma vcg_AssignIdx_retv_wlp_unfold[named_ss vcg_bb]:
    "wlp \<pi> (AssignIdx_retv x i a) Q s = INSERT_NAMING_HINT a x (\<lambda>s. wlp \<pi> (x[i]::=V a) Q s) s"
    unfolding AssignIdx_retv_def INSERT_NAMING_HINT_def by auto
    
  lemma vcg_AssignIdx_retv_wp_unfold[named_ss vcg_bb]:
    "wp \<pi> (AssignIdx_retv x i a) Q s = INSERT_NAMING_HINT a x (\<lambda>s. wp \<pi> (x[i]::=V a) Q s) s"
    unfolding AssignIdx_retv_def INSERT_NAMING_HINT_def by auto
    
  lemma vcg_ArrayCpy_retv_wlp_unfold[named_ss vcg_bb]:
    "wlp \<pi> (ArrayCpy_retv x a) Q s = INSERT_NAMING_HINT a x (\<lambda>s. wlp \<pi> (x[]::=a) Q s) s"  
    unfolding ArrayCpy_retv_def INSERT_NAMING_HINT_def by auto
    
  lemma vcg_ArrayCpy_retv_wp_unfold[named_ss vcg_bb]:
    "wp \<pi> (ArrayCpy_retv x a) Q s = INSERT_NAMING_HINT a x (\<lambda>s. wp \<pi> (x[]::=a) Q s) s"  
    unfolding ArrayCpy_retv_def INSERT_NAMING_HINT_def by auto
    
  text \<open>Unfolding sequential composition\<close>          
  lemmas vcg_seq_unfold[named_ss vcg_bb] 
    = wp_seq_eq wlp_seq_eq
    
  lemmas [named_ss vcg_bb] = wp_skip_eq wlp_skip_eq 

  text \<open>Unfolding if-then-else. We insert a semantic constant, and recursively unfold
    the condition, and the then and else part. \<close>
  definition "VCG_IF_THEN_ELSE B T E Q s \<equiv> if (B s) then (T Q s) else (E Q s)"
  
  lemma wp_if_noexplode_eq:
    "wp \<pi> (IF b THEN c\<^sub>1 ELSE c\<^sub>2) Q s = VCG_IF_THEN_ELSE (\<lambda>s. bval b s) (\<lambda>Q s. (wp \<pi> c\<^sub>1 Q s)) (\<lambda>Q s. (wp \<pi> c\<^sub>2 Q s)) (\<lambda>s. Q s) s"
    by (auto simp: VCG_IF_THEN_ELSE_def wp_if_eq)

  lemma wlp_if_noexplode_eq:
    "wlp \<pi> (IF b THEN c\<^sub>1 ELSE c\<^sub>2) Q s = VCG_IF_THEN_ELSE (\<lambda>s. bval b s) (\<lambda>Q s. (wlp \<pi> c\<^sub>1 Q s)) (\<lambda>Q s. (wlp \<pi> c\<^sub>2 Q s)) (\<lambda>s. Q s) s"
    by (auto simp: VCG_IF_THEN_ELSE_def wlp_if_eq)
    
  lemmas [named_ss vcg_bb] = wp_if_noexplode_eq wlp_if_noexplode_eq
  
  text \<open>Unfolding a scope. Again, we insert a semantic constant, and then recursively unfold.\<close>
  
  definition "VCG_SCOPE C Q s = C (\<lambda>s'. Q <s|s'>) <<>|s>"
  
  lemma wp_scope_noexplode_eq: "wp \<pi> (SCOPE c) Q s = VCG_SCOPE (\<lambda>Q s. wp \<pi> c Q s) (\<lambda>s. Q s) s"
    unfolding VCG_SCOPE_def wp_scope_eq by simp
  
  lemma wlp_scope_noexplode_eq: "wlp \<pi> (SCOPE c) Q s = VCG_SCOPE (\<lambda>Q s. wlp \<pi> c Q s) (\<lambda>s. Q s) s"
    unfolding VCG_SCOPE_def wlp_scope_eq by simp

  lemmas [named_ss vcg_bb] = wp_scope_noexplode_eq wlp_scope_noexplode_eq
  
  text \<open>Unfolding WHILE-loops. We unfold to a semantic constant, but also annotate the 
    set of LHS-variables of the loop body. We also keep track of invariant annotations.
  \<close>
  (* TODO: Having to accomodate for all program analysis information at this point feels odd.
    Maybe, this can be generalized over the kind of program analysis we want to annotate.*)

  lemma VCG_WHILE_wp_aux: "wp \<pi> (WHILE b DO c) 
    = (\<lambda>Q s. \<exists>b' c'. bval b = bval b' \<and> c \<sim> c' \<and> wp \<pi> (WHILE b' DO c') Q s)"  
    apply (intro ext)
    using sim_while_cong wp_equiv by force
    
  lemma VCG_WHILE_wlp_aux: "wlp \<pi> (WHILE b DO c) 
    = (\<lambda>Q s. \<exists>b' c'. bval b = bval b' \<and> c \<sim> c' \<and> wlp \<pi> (WHILE b' DO c') Q s)"  
    apply (intro ext)
    using sim_while_cong wlp_equiv by force

  type_synonym wp_type = "com \<Rightarrow> (state \<Rightarrow> bool) \<Rightarrow> state \<Rightarrow> bool"
    
  definition VCG_WHILE :: "(program \<Rightarrow> wp_type) \<Rightarrow> program \<Rightarrow> 'a rel \<Rightarrow> (state \<Rightarrow> 'a) \<Rightarrow> (state \<Rightarrow> bool) \<Rightarrow> vname set \<Rightarrow> _"
  where
    "VCG_WHILE ptr \<pi> RR VV II modset B C Q s \<equiv> 
      (\<forall>b c. modset = lhsv \<pi> c \<and> B = bval b \<and> C = ptr \<pi> c \<longrightarrow> ptr \<pi> (WHILE b DO c) Q s)"  

  (* Note: The bval and wlp need to be eta-expanded here, such that 
    the simplifier will continue to unfold them! *)          
  lemma vcg_while_wp_gen_eq: "wp \<pi> (WHILE b DO c) Q s 
    = VCG_WHILE wp \<pi> RR VV II (ANALYZE (lhsv \<pi> c)) (\<lambda>s. bval b s) (\<lambda>Q s. wp \<pi> c Q s) Q s"
    (* TODO: Proof can be made more concise *)
    apply (subst VCG_WHILE_wp_aux)
    unfolding VCG_WHILE_def apply auto
    by (metis sim_while_wp wp_equiv)

  lemma vcg_while_wlp_gen_eq: "wlp \<pi> (WHILE b DO c) Q s 
    = VCG_WHILE wlp \<pi> RR VV II (ANALYZE (lhsv \<pi> c)) (\<lambda>s. bval b s) (\<lambda>Q s. wlp \<pi> c Q s) Q s"
    apply (subst VCG_WHILE_wlp_aux)
    unfolding VCG_WHILE_def apply auto
    by (metis sim_while_wlp wlp_equiv)
    
  consts 
    NO_IANNOT :: "state \<Rightarrow> bool"
    NO_VANNOT :: "state \<Rightarrow> unit"  
    NO_RANNOT :: "unit rel"
  
  specification (NO_IANNOT) "NO_IANNOT = NO_IANNOT" by simp
  specification (NO_VANNOT) "NO_VANNOT = NO_VANNOT" by simp
  specification (NO_RANNOT) "NO_RANNOT = NO_RANNOT" by simp
    
  text \<open>Default unfolding rule for unannotated while loops\<close>
  lemmas [named_ss vcg_bb] = 
    vcg_while_wp_gen_eq[of _ _ _ _ _ NO_RANNOT NO_VANNOT NO_IANNOT] 
    vcg_while_wlp_gen_eq[of _ _ _ _ _ NO_RANNOT NO_VANNOT NO_IANNOT]
    
  lemmas wlp_while_annot_eq[named_ss vcg_bb] 
    = vcg_while_wlp_gen_eq[of _ _ _ _ _ NO_RANNOT NO_VANNOT I,folded WHILE_annotI_def[of I]]
    for I :: "state \<Rightarrow> bool"
    
  lemmas wp_while_annotRVI_eq[named_ss vcg_bb] 
    = vcg_while_wp_gen_eq[of _ _ _ _ _ R V I,folded WHILE_annotRVI_def[of R V I]]
    for R :: "'a rel" and V :: "state \<Rightarrow> 'a" and I :: "state \<Rightarrow> bool"
      
  lemmas wp_while_annotVI_eq[named_ss vcg_bb] 
    = vcg_while_wp_gen_eq[of _ _ _ _ _ "measure nat" V I,unfolded annotate_whileVI[of V I]]
    for V :: "state \<Rightarrow> int" and I :: "state \<Rightarrow> bool"
      
  text \<open>The standard annotations should be unfolded during vcg-generation\<close>      
  lemmas [named_ss vcg_bb] = in_inv_image in_measure
    
    
    
    
  definition CALL :: "wp_type \<Rightarrow> _" where "CALL ptr p Q s \<equiv> ptr (PCall p) (\<lambda>s. Q s) s"
  
  lemma vcg_gen_fold_call: 
    "ptr (PCall p) (\<lambda>s. Q s) s = CALL ptr p (\<lambda>s. Q s) s" 
    unfolding CALL_def by simp
    
  lemmas [named_ss vcg_bb] = vcg_gen_fold_call[of "wp _"] vcg_gen_fold_call[of "wlp _"]

  
  definition PARAMS :: "wp_type \<Rightarrow> _" where "PARAMS ptr c Q s \<equiv> ptr c (\<lambda>s. Q s) s"
  
  lemma vcg_gen_fold_params: 
    "ptr (Params c) (\<lambda>s. Q s) s = PARAMS ptr c (\<lambda>s. Q s) s" 
    unfolding PARAMS_def Params_def by simp
    
  lemmas [named_ss vcg_bb] = vcg_gen_fold_params[of "wp _"] vcg_gen_fold_params[of "wlp _"]
  
      
  subsection \<open>Approximation Phase\<close>
      
  text \<open>A marker to indicate where a goal comes from.\<close>
  (* TODO: Noschinski has driven this to the extreme, allowing to generate a 
    complete isar case setup from such annotations. 
    We should adapt (at least parts of) this idea!
  *)
  definition GOAL_INDICATION :: "'a \<Rightarrow> bool" ("\<paragraph>_" [1000])
    where "GOAL_INDICATION _ \<equiv> True"
  
  lemma move_goal_indication_to_front[simp, named_ss vcg_bb]: 
    "NO_MATCH (\<paragraph>x) P \<Longrightarrow> (P\<Longrightarrow>\<paragraph>n\<Longrightarrow>PROP Q) \<equiv> (\<paragraph>n \<Longrightarrow> P \<Longrightarrow> PROP Q)"  
    by (rule swap_prems_eq)
    (* TODO: Would like to use PROP P here, to also move over 
      meta-premises like \<open>\<And>x. a\<Longrightarrow>b\<close>, but \<open>NO_MATCH\<close> requires type! *)
        
  lemma VCG_IF_THEN_ELSEI[vcg_rules]:
    assumes "\<lbrakk> \<paragraph>''then''; B s\<rbrakk> \<Longrightarrow> T Q s"
    assumes "\<lbrakk> \<paragraph>''else''; \<not>B s\<rbrakk> \<Longrightarrow> E Q s"
    shows "VCG_IF_THEN_ELSE B T E Q s"
    using assms
    unfolding VCG_IF_THEN_ELSE_def GOAL_INDICATION_def
    by simp
      
  
  lemma wlp_whileI_modset:
    fixes c \<pi>
    defines [simp]: "modset \<equiv> ANALYZE (lhsv \<pi> c)"
    assumes INIT: "I \<ss>\<^sub>0"
    assumes STEP: "\<And>\<ss>. \<lbrakk> modifies modset \<ss> \<ss>\<^sub>0; I \<ss>; bval b \<ss> \<rbrakk> \<Longrightarrow> wlp \<pi> c (\<lambda>\<ss>'. I \<ss>') \<ss>"
    assumes FINAL: "\<And>\<ss>. \<lbrakk> modifies modset \<ss> \<ss>\<^sub>0; I \<ss>; \<not>bval b \<ss> \<rbrakk> \<Longrightarrow> Q \<ss>"
    shows "wlp \<pi> (WHILE b DO c) Q \<ss>\<^sub>0"
    apply (rule wlp_whileI[where I="\<lambda>\<ss>. I \<ss> \<and> modifies modset \<ss> \<ss>\<^sub>0"])
    subgoal using INIT by simp
    subgoal
      apply (rule wlp_conseq, rule wlp_strengthen_modset, rule STEP)
      apply (auto dest: modifies_trans)
      done
    subgoal using FINAL by simp
    done
    
  lemma wp_whileI_modset:
    fixes c \<pi>
    defines [simp]: "modset \<equiv> ANALYZE (lhsv \<pi> c)"
    assumes WF: "wf R"
    assumes INIT: "I \<ss>\<^sub>0"
    assumes STEP: "\<And>\<ss>. \<lbrakk> modifies modset \<ss> \<ss>\<^sub>0; I \<ss>; bval b \<ss> \<rbrakk> \<Longrightarrow> wp \<pi> c (\<lambda>\<ss>'. I \<ss>' \<and> (\<ss>',\<ss>)\<in>R) \<ss>"
    assumes FINAL: "\<And>\<ss>. \<lbrakk> modifies modset \<ss> \<ss>\<^sub>0; I \<ss>; \<not>bval b \<ss> \<rbrakk> \<Longrightarrow> Q \<ss>"
    shows "wp \<pi> (WHILE b DO c) Q \<ss>\<^sub>0"
    apply (rule wp_whileI[where I="\<lambda>\<ss>. I \<ss> \<and> modifies modset \<ss> \<ss>\<^sub>0" and R=R])
    apply (rule WF)
    subgoal using INIT by simp
    subgoal
      apply (rule wp_conseq, rule wp_strengthen_modset, rule STEP)
      apply (auto dest: modifies_trans)
      done
    subgoal using FINAL by simp
    done
    
    
    
  lemma VCG_WHILE_wlpI[vcg_rules]:
    assumes "\<paragraph>''invar-initial'' \<Longrightarrow> I s\<^sub>0"
    assumes "\<And>s. \<lbrakk> \<paragraph>''invar-preserve''; modifies modset s s\<^sub>0; B s; I s \<rbrakk> \<Longrightarrow> C I s"
    assumes "\<And>s. \<lbrakk> \<paragraph>''invar-post''; modifies modset s s\<^sub>0; \<not>B s; I s \<rbrakk> \<Longrightarrow> Q s"
    shows "VCG_WHILE wlp \<pi> Ra Va I modset B C Q s\<^sub>0"
    using assms unfolding VCG_WHILE_def GOAL_INDICATION_def apply clarsimp
    apply (rule wlp_whileI_modset[where I=I])
    by auto
    
  definition invar_var_goal where
    "invar_var_goal I R s s' \<equiv> I s' \<and> (s',s)\<in>R"
  
  lemma invar_var_goalI[vcg_rules]:
    "invar_var_goal I R s s'" if "\<paragraph>''Invar pres'' \<Longrightarrow> I s'" "\<paragraph>''Var pres'' \<Longrightarrow> (s',s)\<in>R"
    using that unfolding invar_var_goal_def GOAL_INDICATION_def by auto
    
    
    
  lemma VCG_WHILE_wpI[vcg_rules]:
    assumes "\<paragraph>''variant'' \<Longrightarrow> wf R"
    assumes "\<paragraph>''invar-initial'' \<Longrightarrow> I \<ss>\<^sub>0"
    assumes "\<And>\<ss>. \<lbrakk> modifies modset \<ss> \<ss>\<^sub>0; I \<ss>; B \<ss> \<rbrakk> 
      \<Longrightarrow> C (\<lambda>\<ss>'. invar_var_goal I (inv_image R Va) \<ss> \<ss>') \<ss>"
    assumes "\<And>\<ss>. \<lbrakk> \<paragraph>''invar-final''; modifies modset \<ss> \<ss>\<^sub>0; I \<ss>; \<not>B \<ss> \<rbrakk> \<Longrightarrow> Q \<ss>"
    shows "VCG_WHILE wp \<pi> R Va I modset B C Q \<ss>\<^sub>0"
    using assms unfolding VCG_WHILE_def GOAL_INDICATION_def invar_var_goal_def apply clarsimp
    apply (rule wp_whileI_modset[where I=I and R="inv_image R Va"])
    apply auto
    done
    
  lemma VCG_SCOPEI[vcg_rules]:
    assumes "C (\<lambda>s'. Q <s|s'>) <<>|s>"
    shows "VCG_SCOPE C Q s"  
    using assms by (auto simp: VCG_SCOPE_def)
    
  lemma VCG_INSERT_NAMING_HINTI[vcg_rules]:
    assumes "NAMING_HINT s x y \<Longrightarrow> C s"
    shows "INSERT_NAMING_HINT x y C s"
    using assms unfolding NAMING_HINT_def INSERT_NAMING_HINT_def by simp
    
  subsubsection \<open>Inlining\<close>
  
  lemma vcg_wlp_inline_rl[vcg_frame_rules]:
    assumes "HT_partial_mods \<pi> mods P c Q"
    assumes "\<paragraph>(''Inline-Pre'',c) \<Longrightarrow> P s"
    assumes "\<And>s'. \<lbrakk>modifies mods s' s; Q s s'\<rbrakk> \<Longrightarrow> Q' s'"
    shows "wlp \<pi> (Inline c) (\<lambda>s'. Q' s') s"
    using vcg_wlp_conseq assms unfolding Inline_def GOAL_INDICATION_def by auto
  
  lemma vcg_wp_inline_rl[vcg_frame_rules]:
    assumes "HT_mods \<pi> mods P c Q"
    assumes "\<paragraph>(''Inline-Pre'',c) \<Longrightarrow> P s"
    assumes "\<And>s'. \<lbrakk>modifies mods s' s; Q s s'\<rbrakk> \<Longrightarrow> Q' s'"
    shows "wp \<pi> (Inline c) (\<lambda>s'. Q' s') s"
    using vcg_wp_conseq assms unfolding Inline_def GOAL_INDICATION_def by auto
    
  lemma vcg_wlp_wp_inline_rl[vcg_frame_rules]:
    assumes "HT_mods \<pi> mods P c Q"
    assumes "\<paragraph>(''Inline-Pre'',c) \<Longrightarrow> P s"
    assumes "\<And>s'. \<lbrakk>modifies mods s' s; Q s s'\<rbrakk> \<Longrightarrow> Q' s'"
    shows "wlp \<pi> (Inline c) (\<lambda>s'. Q' s') s"
    using vcg_wlp_wp_conseq assms unfolding Inline_def GOAL_INDICATION_def by auto
    
  (* Delay these to where concepts are defined! *)  
  subsubsection \<open>Parameter Passing\<close>  
  lemma vcg_wp_params_rl[vcg_frame_rules]:
    assumes "HT_mods \<pi> mods P c Q"
    assumes "\<paragraph>(''Proc-Pre'',c) \<Longrightarrow> P s"
    assumes "\<And>s'. \<lbrakk>modifies mods s' s; Q s s'\<rbrakk> \<Longrightarrow> Q' s'"
    shows "PARAMS (wp \<pi>) c (\<lambda>s'. Q' s') s"
    using vcg_wp_conseq assms unfolding PARAMS_def Params_def GOAL_INDICATION_def by auto
    
  
  lemma vcg_wlp_params_rl[vcg_frame_rules]:
    assumes "HT_partial_mods \<pi> mods P c Q"
    assumes "\<paragraph>(''Proc-Pre'',c) \<Longrightarrow> P s"
    assumes "\<And>s'. \<lbrakk>modifies mods s' s; Q s s'\<rbrakk> \<Longrightarrow> Q' s'"
    shows "PARAMS (wlp \<pi>) c (\<lambda>s'. Q' s') s"
    using vcg_wlp_conseq assms unfolding PARAMS_def Params_def GOAL_INDICATION_def by auto
    
  
  lemma vcg_wlp_wp_params_rl[vcg_frame_rules]:
    assumes "HT_mods \<pi> mods P c Q"
    assumes "\<paragraph>(''Proc-Pre'',c) \<Longrightarrow> P s"
    assumes "\<And>s'. \<lbrakk>modifies mods s' s; Q s s'\<rbrakk> \<Longrightarrow> Q' s'"
    shows "PARAMS (wlp \<pi>) c (\<lambda>s'. Q' s') s"
    using vcg_wlp_wp_conseq assms unfolding PARAMS_def Params_def GOAL_INDICATION_def by auto

  subsubsection \<open>Recursive Calls\<close>  
  lemma vcg_wp_call_rl[vcg_frame_rules]:
    assumes "HT_mods \<pi> mods P (PCall p) Q"
    assumes "\<paragraph>(''Rec-Pre'',p) \<Longrightarrow> P s"
    assumes "\<And>s'. \<lbrakk> modifies mods s' s; Q s s' \<rbrakk> \<Longrightarrow> Q' s'"
    shows "CALL (wp \<pi>) p Q' s"
    using vcg_wp_conseq[OF assms(1), OF assms(2)] assms(3) 
    unfolding CALL_def GOAL_INDICATION_def
    by blast
    

end
