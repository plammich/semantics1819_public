theory List_Demo
imports Main
begin

term map



datatype 'a list = Nil | Cons "'a" "'a list"

term "Nil"

declare [[names_short]]

fun app :: "'a list \<Rightarrow> 'a list \<Rightarrow> 'a list" where
"app Nil ys = ys" |
"app (Cons x xs) ys = Cons x (app xs ys)"

fun rev :: "'a list \<Rightarrow> 'a list" where
"rev Nil = Nil" |
"rev (Cons x xs) = app (rev xs) (Cons x Nil)"

value "rev(Cons True (Cons False Nil))"

value "rev(Cons a (Cons b Nil))"

lemma [simp]: "app xs Nil = xs" by (induction xs) auto

lemma [simp]: "app (app xs ys) zs = app xs (app ys zs)"
  by (induction xs) auto

lemma rev_app[simp]: "rev (app xs ys) = app (rev ys) (rev xs)" 
  apply (induction xs)
  apply auto
  done

lemma rev_app'[simp]: "app (rev ys) (rev xs) = rev (app xs ys)" 
  by auto

lemma [simp]: " x = x + (0::nat)" by auto  
  

lemma "app (app xs ys) zs = app ys zs"
  apply 
    
theorem rev_rev: "rev (rev xs) = xs"
apply (induction xs)
apply (auto)
done


end
