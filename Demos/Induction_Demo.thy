theory Induction_Demo
imports Main
begin

fun itrev :: "'a list \<Rightarrow> 'a list \<Rightarrow> 'a list" where
"itrev [] ys = ys" |
"itrev (x#xs) ys = itrev xs (x#ys)"


lemma itrev_aux: "itrev xs ys = rev xs @ ys"
  apply(induction xs arbitrary: ys)
  apply(auto)
  done


lemma "itrev xs [] = rev xs"
  by (auto simp: itrev_aux)
  


(* Standard Example: fold *)

thm fold.simps (* Equations on functions *)

(* Equivalent equations, with explicit accumulator *)
lemma 
  "fold f [] a = a"
  "fold f (x#xs) a = fold f xs (f x a)"
  by auto

lemma "fold Cons xs ys = rev xs@ys"  
  by (induction xs arbitrary: ys) auto
  
definition "f xs = fold (+) xs 0"
(* What function is f, intuitively? *)

lemma aux1: "a + fold (+) ys (b::nat) = fold (+) ys (a+b)"
  apply (induction ys arbitrary: b)
  apply (auto simp: algebra_simps)
  done

lemma "fold (+) xs a = fold (+) xs (0::nat) + a"
  apply (induction xs arbitrary: a)
  oops

    
thm fold_append (* Auto will apply this theorem *)
  
lemma "f (xs@ys) = f xs + f (ys::nat list)"
  unfolding f_def
  apply (auto simp: aux1)
  done 



hide_const f (* Make f available as variable name again \<dots> *)

(** Back to slides *)

subsection{* Computation Induction *}

fun sep :: "'a \<Rightarrow> 'a list \<Rightarrow> 'a list" where
"sep a [] = []" |
"sep a [x] = [x]" |
"sep a (x#y#zs) = x # a # sep a (y#zs)"

thm sep.induct

lemma "map f (sep a xs) = sep (f a) (map f xs)"
apply(induction a xs rule: sep.induct)
apply auto
done



fun zipf where
  "zipf f [] [] = []"
| "zipf f (x#xs) (y#ys) = f x y #  zipf f xs ys"  
| "zipf _ _ _ = []"  
  

lemma "length xs = length ys \<Longrightarrow> length (zipf f xs ys) = length ys"
  apply (induction f xs ys rule: zipf.induct)
  by auto


(* Generalization of the well-known zip function *)
lemma "zipf Pair xs ys = zip xs ys"
  (* If parameter is not variable, explicit type specification required! *)
  apply (induction "Pair::'a \<Rightarrow> 'b \<Rightarrow> ('a\<times>'b)" "xs::'a list" "ys::'b list" rule: zipf.induct)
  by auto
  

  
  


end
