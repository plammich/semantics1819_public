section "Stack Machine and Compilation"

theory ASM
imports "Complete/AExp"
begin

subsection "Stack Machine"

datatype instr = LOADI val | LOAD vname | ADD

type_synonym stack = "val list"

fun exec1 :: "instr \<Rightarrow> state \<Rightarrow> stack \<Rightarrow> stack" where
"exec1 (LOADI n) _ stk  =  n # stk" |
"exec1 (LOAD x) s stk  =  s x # stk" |
"exec1  ADD _ (i # j # stk)  = (i+j) # stk "

fun exec :: "instr list \<Rightarrow> state \<Rightarrow> stack \<Rightarrow> stack" where
"exec [] _ stk = stk" |
"exec (i#is) s stk = exec is s (exec1 i s stk)"

lemma "exec is s stk = fold (\<lambda>i stk. exec1 i s stk) is stk"
  apply (induction "is" arbitrary: stk) apply auto done 

value "exec [LOADI 5, LOAD ''y'', ADD] <''x'' := 42, ''y'' := 43> [50]"


subsection "Compilation"

fun comp :: "aexp \<Rightarrow> instr list" where
"comp (N n) = [LOADI n]" |
"comp (V x) = [LOAD x]" |
"comp (Plus e1 e2) = comp e1 @ comp e2 @ [ADD]"

value "comp (Plus (Plus (V ''x'') (N 1)) (V ''z''))"

term Let
term "Let v (\<lambda>x. f)"

term If
term "If b t "

lemma "exec (is1@is2) s stk = (let stk = exec is1 s stk; stk = exec is2 s stk in stk)"
  apply (induction is1 arbitrary: stk)
  apply (auto simp: Let_def) (* Explicitly inline let. Not necessary here*)
  done
  

lemma exec_append[simp]: "exec (is1@is2) s stk = exec is2 s (exec is1 s stk)"
  apply (induction is1 arbitrary: stk)
  apply (auto) 
  done
  
theorem exec_comp_aux: "exec (comp a) s stk = aval a s # stk"
  apply (induction a arbitrary: stk)
  apply auto
  done


theorem exec_comp: "exec (comp a) s [] = [aval a s]"
  by (simp add: exec_comp_aux) 


end
