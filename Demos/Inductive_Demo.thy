theory Inductive_Demo
imports Main
begin

subsection{*Inductive definition of the even numbers*}

inductive ev :: "nat \<Rightarrow> bool" where
ev0: "ev 0" |
evSS: "ev n \<Longrightarrow> ev (Suc(Suc n))"

thm ev0 evSS
thm ev.intros

text{* Using the introduction rules: *}
lemma "ev (Suc(Suc(Suc(Suc 0))))"
  apply (rule evSS)
  apply (rule evSS)
  by (rule ev0)

thm ev0
thm evSS[OF ev0]
thm evSS[OF evSS[OF ev0]]
  
thm evSS[OF evSS[OF ev0]]

text{* A recursive definition of evenness: *}
fun evn :: "nat \<Rightarrow> bool" where
"evn 0 = True" |
"evn (Suc 0) = False" |
"evn (Suc(Suc n)) = evn n"

text{*A simple example of rule induction: *}
lemma "ev n \<Longrightarrow> evn n"
apply(induction rule: ev.induct)
by auto

text{* An induction on the computation of evn: *}
lemma "evn n \<Longrightarrow> ev n"
apply(induction n rule: evn.induct)
apply (auto intro: ev.intros)

done

text{* No problem with termination because the premises are always smaller
than the conclusion: *}
declare ev.intros[simp,intro]

text{* A shorter proof: *}
lemma "evn n \<Longrightarrow> ev n"
apply(induction n rule: evn.induct)
apply(simp_all)
done

text{* The power of arith: *}
lemma "ev n \<Longrightarrow> \<exists>k. n = 2*k"
apply(induction rule: ev.induct)
apply auto
apply arith
done


subsection{*Inductive definition of the reflexive transitive closure *}

inductive
  star :: "('a \<Rightarrow> 'a \<Rightarrow> bool) \<Rightarrow> 'a \<Rightarrow> 'a \<Rightarrow> bool"
for r where
refl:  "star r x x" |
step:  "r x y \<Longrightarrow> star r y z \<Longrightarrow> star r x z"

lemma star_trans:
  "star r x\<^sub>1 x\<^sub>2 \<Longrightarrow> star r x\<^sub>2 x\<^sub>3 \<Longrightarrow> star r x\<^sub>1 x\<^sub>3"
apply(induction rule: star.induct)
apply(assumption)
apply(rename_tac x\<^sub>1 x' x\<^sub>2)
apply (rule step)
apply assumption
apply assumption
done

end
