theory Fixed_Points
  imports Main
begin

term lfp thm lfp_def
thm lfp_unfold
thm lfp_greatest

definition lfp where "lfp f = \<Inter>{P. f P \<subseteq> P}"


lemma knaster_tarski:
  assumes MONO: "\<And>x y. x\<subseteq>y \<Longrightarrow> f x \<subseteq> f y"
  shows "f (lfp f) = lfp f" 
    and "f x = x \<Longrightarrow> lfp f \<subseteq> x" 
proof -
  have lower: "lfp f \<subseteq> x" if "f x \<subseteq> x" for x using that unfolding lfp_def by auto
  have greatest: "x \<subseteq> lfp f" if "\<forall>P. f P \<subseteq> P \<longrightarrow> x \<subseteq> P" for x
    using that unfolding lfp_def by auto

  have "f (lfp f) \<subseteq> lfp f"
    apply (rule greatest, intro allI impI)
    apply (rule order_trans[rotated], assumption)
    apply (rule MONO)
    apply (rule lower)
    .
  also have "lfp f \<subseteq> f (lfp f)"
    apply (rule lower)
    apply (rule MONO)
    by fact 
  finally show "f (lfp f) = lfp f" .    

  assume A: "f x = x"
  show "lfp f \<subseteq> x"
    apply (rule lower) using A by simp
    
qed  
  
  
lemma knaster_tarski_isar:
  assumes MONO: "\<And>x y. x\<subseteq>y \<Longrightarrow> f x \<subseteq> f y"
  shows "f (lfp f) = lfp f" 
    and "f x = x \<Longrightarrow> lfp f \<subseteq> x" 
proof -          
  have lower: "lfp f \<subseteq> x" if "f x \<subseteq> x" for x using that unfolding lfp_def by auto
  have greatest: "x \<subseteq> lfp f" if "\<forall>P. f P \<subseteq> P \<longrightarrow> x \<subseteq> P" for x
    using that unfolding lfp_def by auto
    
  have PFP: "f (lfp f) \<subseteq> lfp f"  
  proof (rule greatest, intro allI impI)
    fix P assume "f P \<subseteq> P"
    then have "lfp f \<subseteq> P" using lower by blast
    then have "f (lfp f) \<subseteq> f P" by (rule MONO)
    also note \<open>f P \<subseteq> P\<close> 
    finally show "f (lfp f) \<subseteq> P" .
  qed    
  also  
  from lower[OF MONO[OF PFP]] have "lfp f \<subseteq> f (lfp f)" .
  finally show "f (lfp f) = lfp f" . 

  assume A: "f x = x"
  hence "f x \<subseteq> x" by simp
  from lower[OF this] show "lfp f \<subseteq> x" .
qed  
        

lemma compute_lfp:
  assumes MONO: "\<And>x y. x\<subseteq>y \<Longrightarrow> f x \<subseteq> f y"
  assumes STABILIZE: "(f^^(Suc k)) {} = (f^^k) {}"
  shows "(f^^k) {} = lfp f"
proof
  have lower: "lfp f \<subseteq> x" if "f x \<subseteq> x" for x using that unfolding lfp_def by auto
  from STABILIZE have "f ((f^^k) {}) \<subseteq> (f^^k) {}" by auto
  from lower[OF this] show "lfp f \<subseteq> (f^^k) {}" . 
next
  show "(f^^k) {} \<subseteq> lfp f"
  proof (induction k)
    case 0
    have "(f^^0) {} = {}" by simp
    also have "{} \<subseteq> lfp f" ..
    finally show ?case .
  next
    case (Suc k)
    from MONO[OF Suc.IH] have "(f^^Suc k) {} \<subseteq> f (lfp f)" by simp
    also from knaster_tarski(1)[OF MONO] have "f (lfp f) = lfp f" .
    finally show ?case .
  qed
qed  






end
