theory tut08
  imports
    "IMP2.Examples"
begin

lemma aux:
  "x * (x * x) ^ nat (n div 2) = x ^ nat n" if "n mod 2 = 1" "n > 0" for x :: int
proof -
  have *: "2 * nat (n div 2) = nat n - 1"
    by (smt even_of_nat int_nat_eq nat_2 nat_div_distrib numeral_2_eq_2 odd_iff_mod_2_eq_one odd_two_times_div_two_nat that(1) that(2))
  have "x * x = x ^ 2"
    by (simp add: power2_eq_square)
  then have "(x * x) ^ nat (n div 2) = x ^ (2 * nat (n div 2))"
    apply simp
    using semiring_normalization_rules(31) by blast
  then show ?thesis
    apply (simp add: *)
    by (metis One_nat_def power_commutes power_minus_mult that(2) zero_less_nat_eq)
qed

program_spec ex_by_sq
  assumes "n\<ge>0"
  ensures "r = x\<^sub>0 ^ nat n\<^sub>0"
defines \<open>
    r = 1;
    while (n\<noteq>0) 
      @invariant \<open>
        n \<ge> 0 \<and> r * (x ^ nat n) = x\<^sub>0 ^ nat n\<^sub>0
      \<close>
      @variant \<open>nat n\<close>
    {
      if (n mod 2 == 1) {
        r = r * x
      };
      x = x * x;
      n = n / 2
    }
  \<close>
  apply vcg_cs
  subgoal
    apply (subst (asm) aux[symmetric])
      apply (auto simp: algebra_simps)
    done
   apply (simp add: semiring_normalization_rules)
  apply (simp add: nat_mult_distrib semiring_normalization_rules)
  done


program_spec find
  assumes "l \<le> h"
  ensures "if l = h then (\<forall> i \<in> {l\<^sub>0..<h\<^sub>0}. a i \<noteq> x) else (a l = x)"
defines \<open>
  while (l < h \<and> a[l] \<noteq> x)
  @invariant \<open>l \<le> h \<and> l\<^sub>0 \<le> l \<and> (\<forall> i \<in> {l\<^sub>0..<l}. a i \<noteq> x)\<close>
  @variant \<open>nat (h - l)\<close>
  {
    l = l + 1
  }
\<close>
  apply vcg_cs
  apply auto
  done

end