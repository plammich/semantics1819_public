(*<*)
theory ex08
  imports
    "IMP2.Examples"
begin
(*>*)

text \<open>\ExerciseSheet{8}{04.12.2018}\<close>

text \<open>\Exercise{Exponentiation by Squaring}\<close>
(*<*)
lemma aux:
  fixes x :: int and n :: nat
  assumes "n mod 2 = 1"
  shows "x * (x * x) ^ (n div 2) = x ^ n"
proof -
  have "n > 0"
    using assms by presburger
  have "2 * (n div 2) = n - 1"
    using assms by presburger
  then have "(x * x) ^ (n div 2) = x ^ (n - 1)"
    by (simp add: semiring_normalization_rules)
  with \<open>0 < n\<close> show ?thesis
    by simp (metis Suc_pred power.simps(2))
qed
(*>*)

text \<open>A classic algorithm for computing \<open>x\<^sup>n\<close> works by repeated squaring,
using the following recurrence:
  \<^item> \<open>x\<^sup>n = x * x\<^bsup>(n-1)/2\<^esup>\<close> if \<open>n\<close> is odd
  \<^item> \<open>x\<^sup>n = x\<^bsup>n/2\<^esup>\<close> if \<open>n\<close> is even

Below you find an implementation of this algorithm. Prove that it fulfills the specification!
\<close>
program_spec ex_by_sq
  assumes "n\<ge>0"
  ensures "r = x\<^sub>0 ^ nat n\<^sub>0"
defines \<open>
    r = 1;
    while (n\<noteq>0) 
      @invariant \<open>
        n \<ge> 0 \<and> r * x ^ nat n = x\<^sub>0 ^ nat n\<^sub>0
      \<close>
      @variant \<open>nat n\<close>
    {
      if (n mod 2 == 1) {
        r = r * x
      };
      x = x * x;
      n = n / 2
    }
  \<close>
(*<*)
  apply vcg_cs
  subgoal
    apply (subst (asm) aux[symmetric])
     apply (smt Suc_1 nat_1 nat_2 nat_mod_distrib nat_one_as_int)
    by (simp add: nat_div_distrib)
   apply (simp; fail)
  apply (simp add: algebra_simps semiring_normalization_rules nat_mult_distrib; fail)
  done
(*>*)

text \<open>\Exercise{Finding the Least Index of an Element}\<close>
text \<open>Write a program that checks whether a certain element \<open>x\<close> is contained in an array \<open>a\<close>.
If \<open>a\<close> contains \<open>x\<close>, your program should ``return'' the \<^emph>\<open>least\<close> index at which \<open>x\<close> can be found
in some variable.
Otherwise your program should set the same variable to the array upper bound.
Specify this property formally and prove that your program fulfills this property.
\<close>
(*<*)
program_spec find_least_idx
  assumes \<open>l\<le>h\<close>
  ensures \<open>if l=h\<^sub>0 then x\<^sub>0 \<notin> a\<^sub>0`{l\<^sub>0..<h\<^sub>0} else l\<in>{l\<^sub>0..<h\<^sub>0} \<and> a\<^sub>0 l = x\<^sub>0 \<and> x\<^sub>0\<notin>a\<^sub>0`{l\<^sub>0..<l} \<close>
  defines \<open>
    while (l<h \<and> a[l] \<noteq> x) 
      @variant \<open> nat (h-l) \<close>
      @invariant \<open>l\<^sub>0\<le>l \<and> l\<le>h \<and> x\<notin>a`{l\<^sub>0..<l}\<close>
      l=l+1
  \<close>
  apply vcg_cs
  by (smt atLeastLessThan_iff imageI)
(*>*)

text \<open>\Exercise{Right Rotation}\<close>
text \<open>Write down and verify a program that rotates an array to the right.
That is, your program should fulfill the following specification:
\<close>

program_spec rotate_right
  assumes "0<n"
  ensures "\<forall>i\<in>{0..<n}. a i = a\<^sub>0 ((i-1) mod n)"
(*<*)  
  defines \<open>
    i = 0;
    prev = a[n - 1];
    while (i < n)
      @invariant \<open>
        0 \<le> i \<and> i \<le> n 
        \<and> (\<forall>j\<in>{0..<i}. a j = a\<^sub>0 ((j-1) mod n))
        \<and> (\<forall>j\<in>{i..<n}. a j = a\<^sub>0 j)
        \<and> prev = a\<^sub>0 ((i-1) mod n)
      \<close>
      @variant \<open>nat (n - i)\<close>
    {
      temp = a[i];
      a[i] = prev;
      prev = temp;
      i = i + 1
    }
    \<close>
  apply vcg_cs
  apply (simp add: zmod_minus1)
  done
(*>*)  

text \<open>Hint: @{thm zmod_minus1[no_vars]} (\<open>zmod_minus1\<close>)!\<close>

(*<*)
end
(*>*)