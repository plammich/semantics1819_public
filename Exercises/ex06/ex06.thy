(*<*)
theory ex06
imports IMP.Wp_Demo
begin
declare [[names_short]]
(*>*)
text \<open>\ExerciseSheet{6}{20.11.2018}\<close>

text \<open>
  \Exercise{Weakest Preconditions}

  In this exercise, you shall prove the correctness of two simple programs using
  weakest preconditions.
\<close>
paragraph "Step 1"

text \<open>
  Write a program that stores the maximum of the values of variables
  @{text "a"} and @{text "b"} in variable @{text "c"}.
\<close>
definition Max :: com where
(*<*)
  "Max \<equiv>
  IF (Less (V ''a'') (V ''b'')) THEN
    ''c''::=V ''b''
  ELSE
    ''c''::=V ''a''"
(*>*)

paragraph "Step 2"

text \<open>Prove these lemmas about @{const max}:\<close>
lemma [simp]: "(a::int)<b \<Longrightarrow> max a b = b"
  (*<*)by (metis order_less_imp_le sup_absorb2 sup_int_def)(*>*)
                              
lemma [simp]: "\<not>(a::int)<b \<Longrightarrow> max a b = a"
  (*<*)by auto(*>*)

text \<open>Show total correctness of @{const "Max"}:\<close>
lemma "wp Max (\<lambda>s. s ''c'' = max (s ''a'') (s ''b'')) s"
(*<*)
(*
  unfolding Max_def
  unfolding wp_eq'
  by (simp split: if_split)
*)
  unfolding Max_def
  unfolding wp_if_eq
  unfolding wp_seq_eq
  apply (simp split: if_split)
  unfolding wp_assign_eq
  apply simp
  done
(*>*)

paragraph "Step 3"

text \<open>
  Note that our specification still has a problem, as
  programs are allowed to overwrite arbitrary variables.

  For example, regard the following (wrong) implementation of @{const "Max"}:
\<close>
definition "MAX_wrong = (''a''::=N 0;;''b''::=N 0;;''c''::= N 0)"

text \<open>Prove that @{const "MAX_wrong"} also satisfies the specification for @{const "Max"}:\<close>

lemma "wp MAX_wrong (\<lambda>s. s ''c'' = max (s ''a'') (s ''b'')) s"
(*<*)
  unfolding MAX_wrong_def wp_eq' by (simp split: if_split)
(*>*)

text \<open>What we really want to specify is, that @{const "Max"} computes the
  maximum of the values of @{text "a"} and @{text "b"} in the initial state.
  Moreover, we may require that @{text "a"} and @{text "b"} are not changed.

  For this, we can use logical variables in the specification.
  Prove the following more accurate specification for @{const "Max"}:
\<close>
lemma "a=s ''a'' \<and> b=s ''b'' \<Longrightarrow> wp Max (\<lambda>s. s ''c'' = max a b \<and> a = s ''a'' \<and> b = s ''b'') s"
(*<*)
  unfolding Max_def wp_eq' by (simp split: if_split)
(*>*)

paragraph "Step 4"

text \<open>
  Write a program that calculates the sum of the first \<open>n\<close> natural numbers.
  The parameter \<open>n\<close> is given in the variable \<open>n\<close>.
\<close>
definition Sum :: com where
(*<*)
  "Sum \<equiv>
  ''x'' ::= N 0;;
  WHILE Less (N 0) (V ''n'') DO (
    ''x'' ::= Plus (V ''x'') (V ''n'');;
    ''n'' ::= Plus (V ''n'') (N (-1)))
  "
(*>*)

paragraph "Step 5"

text \<open>Find a proposition that states \<^emph>\<open>partial\<close> correctness of @{const "Sum"} and prove it!
\<^emph>\<open>Hint:\<close> Use the following specification for the sum of the first \<open>n\<close> non-negative integers.\<close>

fun sum :: "int \<Rightarrow> int" where
"sum i = (if i \<le> 0 then 0 else sum (i - 1) + i)"

lemma sum_simps[simp]:
  "0 < i \<Longrightarrow> sum i = sum (i - 1) + i"
  "i \<le> 0 \<Longrightarrow> sum i = 0"
  by simp+

lemmas [simp del] = sum.simps

(*<*)
lemma sum_correct:
  "wlp Sum (\<lambda>s'. s' ''x'' = sum (s ''n'')) s"
  unfolding Sum_def
  unfolding wlp_eq
  by (smartvcg vcg_rules: wlp_whileI[where I = "\<lambda>s'. s' ''x'' = sum (s ''n'') - sum (s' ''n'')"])
(*>*)

text \<open>\Exercise{Forward Assignment Rule}

  Think up and prove a forward assignment rule, i.e., a rule of the
  form @{term "P s \<Longrightarrow> wp (x::=a) Q s"}, where @{term Q} is some suitable
  postcondition.
\<close>
(*<*)
lemma
  fwd_Assign: "P s \<Longrightarrow> wp (x::=a) (\<lambda>s'. \<exists>s. P s \<and> s'=s(x := aval a s)) s"
  unfolding wp_eq by auto
  (*>*)

lemmas fwd_Assign' = wp_conseq[OF fwd_Assign]

text \<open>Redo the proofs for @{const "Max"} from the previous
  exercise, this time using your forward assignment rule.
\<close>

lemma "wp Max (\<lambda>s. s ''c'' = max (s ''a'') (s ''b'')) s"
(*<*)
  unfolding Max_def
  apply (rule wp_ifI; clarsimp)
   apply (rule fwd_Assign'; auto)
  apply (rule fwd_Assign', assumption)
  apply clarsimp
  done
(*>*)

(*<*)
end
(*>*)