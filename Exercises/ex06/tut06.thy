theory tut06
  imports IMP.Wp_Demo
begin

definition Max :: com where
  "Max \<equiv>
    IF Less (V ''a'') (V ''b'') THEN ''c'' ::= V ''b'' ELSE ''c'' ::= V ''a''"

lemma [simp]: "(a::int)<b \<Longrightarrow> max a b = b"
  by auto
lemma [simp]: "\<not>(a::int)<b \<Longrightarrow> max a b = a"
  by auto

lemma "wp Max (\<lambda>s'. s' ''c'' = max (s' ''a'') (s' ''b'')) s"
  unfolding Max_def
  unfolding wp_if_eq wp_assign_eq
  by simp

definition "MAX_wrong = (''a''::=N 0;;''b''::=N 0;;''c''::= N 0)"

lemma "wp MAX_wrong (\<lambda>s. s ''c'' = max (s ''a'') (s ''b'')) s"
  unfolding MAX_wrong_def
  unfolding wp_seq_eq
  unfolding wp_assign_eq
  apply simp
  done

lemma "a = s ''a'' \<Longrightarrow> b = s ''b''
  \<Longrightarrow> wp Max (\<lambda>s'. s' ''c'' = max a b \<and> s ''a'' = a \<and> s ''b'' = b) s"
  unfolding Max_def by smartvcg

definition Sum :: com where
  "Sum \<equiv>
    ''x'' ::= N 0;;
    WHILE Less (N 0) (V ''n'') DO (
      ''x'' ::= Plus (V ''x'') (V ''n'');;
      ''n'' ::= Plus (V ''n'') (N (-1))
    )"

fun sum :: "int \<Rightarrow> int" where
  "sum i = (if i \<le> 0 then 0 else sum (i - 1) + i)"

lemma sum_simps[simp]:
  "0 < i \<Longrightarrow> sum i = sum (i - 1) + i" "i \<le> 0 \<Longrightarrow> sum i = 0"
  by simp+

lemmas [simp del] = sum.simps

lemma
  "wlp Sum (\<lambda>s'. s' ''x'' = sum (s ''n'')) s"
  unfolding Sum_def
  unfolding wlp_eq
apply (subst wlp_annotate_while[where 
          I="\<lambda>s'. s' ''x'' = sum (s ''n'') - sum (s' ''n'')" 
        ])
  apply smartvcg
  done

lemma
 fwd_Assign:
 "P s \<Longrightarrow> wp (x::=a) (\<lambda>s'. s'=s(x := aval a s)) s"
  unfolding wp_assign_eq
  apply auto
  done

lemmas fwd_Assign' = wp_conseq[OF fwd_Assign]

lemma "a = s ''a'' \<Longrightarrow> b = s ''b''
  \<Longrightarrow> wp Max (\<lambda>s'. s' ''c'' = max a b \<and> s ''a'' = a \<and> s ''b'' = b) s"
  unfolding Max_def
  apply (rule wp_ifI)
  apply (rule fwd_Assign'; simp)+
  done

end