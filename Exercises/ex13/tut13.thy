theory tut13
imports "IMP.Small_Step" "IMP.Def_Init"
begin

fun ev and odd where
  "ev 0 = True" |
  "ev (Suc n) = odd n" |
  "odd 0 = False" |
  "odd (Suc n) = ev n"

fun AA :: "com \<Rightarrow> (vname \<times> aexp) set \<Rightarrow> (vname \<times> aexp) set" where
  "AA SKIP A = A" |
  "AA (x ::= a) A = (if x \<in> vars a then {} else {(x, a)})
    \<union> {(x', a'). (x', a') \<in> A \<and> x \<notin> {x'} \<union> vars a'}" |
  "AA (c\<^sub>1;; c\<^sub>2) A = (AA c\<^sub>2 \<circ> AA c\<^sub>1) A" |
  "AA (IF b THEN c\<^sub>1 ELSE c\<^sub>2) A = AA c\<^sub>1 A \<inter> AA c\<^sub>2 A" |
  "AA (WHILE b DO c) A = A \<inter> AA c A"

fun gen :: "com \<Rightarrow> (vname \<times> aexp) set" and kill :: "com \<Rightarrow> (vname \<times> aexp) set" where
  "gen SKIP = {}" |
  "kill SKIP = {}" |
  "gen (x ::= a) = {(x, a)}" |
  "kill (x ::= a) = {(x', a'). x = x' \<and> a \<noteq> a' \<or> x \<in> vars a'}" |
  "gen (c\<^sub>1;; c\<^sub>2) = (gen c\<^sub>1 - kill c\<^sub>1) \<union> gen c\<^sub>2" |
  "kill (c\<^sub>1;; c\<^sub>2) = (kill c\<^sub>1 - gen c\<^sub>2) \<union> kill c\<^sub>2" |
  "gen (IF b THEN c\<^sub>1 ELSE c\<^sub>2) = gen c\<^sub>1 \<inter> gen c\<^sub>2" |
  "kill (IF b THEN c\<^sub>1 ELSE c\<^sub>2) = kill c\<^sub>1 \<union> kill c\<^sub>2" |
  "gen (WHILE b DO c) = {}" |
  "kill (WHILE b DO c) = kill c"

lemma AA_gen_kill: "AA c A = (A \<union> gen c) - kill c"
  by (induction c arbitrary: A) auto

lemma AA_idemp:
  "AA c (AA c A) = AA c A"
  by (auto simp: AA_gen_kill)

theorem AA_sound_aux:
  assumes "(c, s) \<Rightarrow> s'" "\<forall>(x, a) \<in> A. s x = aval a s"
  shows "\<forall>(x, a) \<in> AA c A. s' x = aval a s'"
  using assms
proof (induction arbitrary: A rule: big_step_induct)
  case (Seq c\<^sub>1 s\<^sub>1 s\<^sub>2 c\<^sub>2 s\<^sub>3)
  then show ?case
    by fastforce
next
  case (WhileTrue b s\<^sub>1 c s\<^sub>2 s\<^sub>3)
  from WhileTrue.IH(1)[OF WhileTrue.prems] have "\<forall>(x, a)\<in>AA c A. s\<^sub>2 x = aval a s\<^sub>2" .
  from WhileTrue.IH(2)[OF this] have "\<forall>(x, a)\<in>AA (WHILE b DO c) (AA c A). s\<^sub>3 x = aval a s\<^sub>3" .
  then show ?case
    by (simp add: AA_idemp)
qed auto

theorem AA_sound:
  "(c, s) \<Rightarrow> s' \<Longrightarrow> \<forall>(x, a) \<in> AA c {}. s' x = aval a s'"
  by (rule AA_sound_aux, assumption, simp)

end