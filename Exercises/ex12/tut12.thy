theory tut12
  imports "IMP.Types"
begin

fun atype :: "tyenv \<Rightarrow> aexp \<Rightarrow> ty option" where
  "atype \<Gamma> (Ic _) = Some Ity" |
  "atype \<Gamma> (Rc _) = Some Rty" |
  "atype \<Gamma> (V x) = Some (\<Gamma> x)" |
  "atype \<Gamma> (Plus a b) = (case (atype \<Gamma> a, atype \<Gamma> b) of
    (Some \<tau>1, Some \<tau>2) \<Rightarrow> (if \<tau>1 = \<tau>2 then Some \<tau>1 else None)
   | _ \<Rightarrow> None)"

fun bok :: "tyenv \<Rightarrow> bexp \<Rightarrow> bool" where
  "bok \<Gamma> (Bc _) = True" |
  "bok \<Gamma> (Not b) = bok \<Gamma> b" |
  "bok \<Gamma> (And a b) \<longleftrightarrow> bok \<Gamma> a \<and> bok \<Gamma> b" |
  "bok \<Gamma> (Less a1 a2) = (case (atype \<Gamma> a1, atype \<Gamma> a2) of
   (Some \<tau>1, Some \<tau>2) \<Rightarrow> \<tau>1 = \<tau>2
  | _ \<Rightarrow> False)"

fun cok :: "tyenv \<Rightarrow> com \<Rightarrow> bool" where
  "cok \<Gamma> SKIP = True" |
  "cok \<Gamma> (x ::= a) = (atype \<Gamma> a = Some (\<Gamma> x))" |
  "cok \<Gamma> (c\<^sub>1;;c\<^sub>2) = (cok \<Gamma> c\<^sub>1 \<and> cok \<Gamma> c\<^sub>2)" |
  "cok \<Gamma> (IF b THEN c\<^sub>1 ELSE c\<^sub>2) = (bok \<Gamma> b \<and> cok \<Gamma> c\<^sub>1 \<and> cok \<Gamma> c\<^sub>2)" |
  "cok \<Gamma> (WHILE b DO c) = (bok \<Gamma> b \<and> cok \<Gamma> c)"

lemma atyping_atype: "(\<Gamma> \<turnstile> a : \<tau>) = (atype \<Gamma> a = Some \<tau>)"
  apply (induction a)
  apply (auto split: option.splits)
  done

lemma btyping_bok: "(\<Gamma> \<turnstile> b) = bok \<Gamma> b"
  apply (induction b)
     apply (auto split: option.splits simp: atyping_atype)
  done

lemma ctyping_cok: "(\<Gamma> \<turnstile> c) = cok \<Gamma> c"
  apply (induction c)
  apply (auto simp: atyping_atype btyping_bok)
  done

end