(*<*)
theory ex12
  imports "IMP.Types"
begin
(*>*)

text \<open>\ExerciseSheet{12}{15.~01.~2019}\<close>

text \<open>\Exercise{Type checker as recursive functions}

Reformulate the inductive predicates \ @{prop "\<Gamma> \<turnstile> a : \<tau>"},\ @{prop "\<Gamma> \<turnstile> (b::bexp)"} \ and \ 
\mbox{@{prop "\<Gamma> \<turnstile> (c::com)"}} \ as three recursive functions
\<close>

fun atype :: "tyenv \<Rightarrow> aexp \<Rightarrow> ty option" 
(*<*)(*keep*)where(*>*)
(*<*)
"atype \<Gamma> (Ic i) = Some Ity" |
"atype \<Gamma> (Rc r) = Some Rty" |
"atype \<Gamma> (V x)  = Some(\<Gamma> x)" |
"atype \<Gamma> (Plus a1 a2) = (case (atype \<Gamma> a1, atype \<Gamma> a2) of
   (Some \<tau>1, Some \<tau>2) \<Rightarrow> if \<tau>1 = \<tau>2 then Some \<tau>1 else None | _ \<Rightarrow> None)"
(*>*)

fun bok :: "tyenv \<Rightarrow> bexp \<Rightarrow> bool" (*<*)(*keep*)where(*>*)
(*<*)
"bok \<Gamma> (Bc v) = True" |
"bok \<Gamma> (Not b) = bok \<Gamma> b" |
"bok \<Gamma> (And b1 b2) = (bok \<Gamma> b1 \<and> bok \<Gamma> b2)" |
"bok \<Gamma> (Less a1 a2) = (case (atype \<Gamma> a1, atype \<Gamma> a2) of
   (Some \<tau>1, Some \<tau>2) \<Rightarrow> \<tau>1 = \<tau>2 | _  \<Rightarrow> False)"
(*>*)

fun cok :: "tyenv \<Rightarrow> com \<Rightarrow> bool" (*<*)(*keep*)where(*>*)
(*<*)
"cok \<Gamma> SKIP = True" |
"cok \<Gamma> (x ::= a) =
    (case atype \<Gamma> a of None \<Rightarrow> False | Some \<tau> 
\<Rightarrow> \<tau> = \<Gamma> x)" |
"cok \<Gamma> (c1;;c2) = (cok \<Gamma> c1 \<and> cok \<Gamma> c2)" |
"cok \<Gamma> (IF b THEN c1 ELSE c2) = (bok \<Gamma> b \<and> cok \<Gamma> c1 \<and> cok \<Gamma> c2)" |
"cok \<Gamma> (WHILE b DO c) = (bok \<Gamma> b \<and> cok \<Gamma> c)"
(*>*)

text \<open>and prove:\<close>
lemma atyping_atype: "(\<Gamma> \<turnstile> a : \<tau>) = (atype \<Gamma> a = Some \<tau>)"
(*<*)by(induction a)(auto split: option.splits)(*>*)

lemma btyping_bok: "(\<Gamma> \<turnstile> b) = bok \<Gamma> b"
(*<*)by(induction b)(auto simp: atyping_atype split: option.splits)(*>*)

lemma ctyping_cok: "(\<Gamma> \<turnstile> c) = cok \<Gamma> c"
(*<*)by (induction c) (auto simp: atyping_atype btyping_bok split: option.splits)(*>*)

(*<*)
end
(*>*)