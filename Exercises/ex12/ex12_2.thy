(*<*)
theory ex12_2
  imports "IMP.Compiler"
begin
(*>*)

text \<open>\Exercise{Compiler optimization}

A common programming idiom is \<open>IF b THEN c\<close>, i.e.,
the else-branch consists of a single @{term SKIP} command.

\begin{enumerate}
\item
Look at how the program 
  @{term "IF Less (V ''x'') (N 5) THEN ''y'' ::= N 3 ELSE SKIP"}
is compiled by \<open>ccomp\<close> and identify a possible compiler optimization.
\item
Implement an optimized compiler (by modifying @{term ccomp}) which reduces the number of instructions
for programs of the form \<open>IF b THEN c\<close>.
\item
Extend the proof
of \<open>ccomp_bigstep\<close> to your modified compiler.
\end{enumerate}
\<close>

(*<*)
value "ccomp (IF Less (V ''x'') (N 5) THEN ''y'' ::= N 3 ELSE SKIP)"


fun ccomp2 :: "com \<Rightarrow> instr list" where
"ccomp2 SKIP = []" |
"ccomp2 (x ::= a) = acomp a @ [STORE x]" |
"ccomp2 (c1;; c2) = ccomp2 c1 @ ccomp2 c2" |
"ccomp2 (IF b THEN c1 ELSE c2) = 
  (if c2 = SKIP
   then (let cc1 = ccomp2 c1; cb = bcomp b False (size cc1) in cb @ cc1)
   else (let cc1 = ccomp2 c1; cc2 = ccomp2 c2; cb = bcomp b False (size cc1 + 1)
   in cb @ cc1 @ JMP (size cc2) # cc2))" |
"ccomp2 (WHILE b DO c) =
  (let cc = ccomp2 c; cb = bcomp b False (size cc + 1)
   in cb @ cc @ [JMP (-(size cb + size cc + 1))])"


value "ccomp2 (IF Less (V ''x'') (N 5) THEN ''y'' ::= N 3 ELSE SKIP)"

lemma "(c,s) \<Rightarrow> t \<Longrightarrow> ccomp2 c \<turnstile> (0,s,stk) \<rightarrow>* (size(ccomp2 c),t,stk)"
proof(induct arbitrary: stk rule: big_step_induct)
  case (Assign x a s)
  show ?case
    using acomp_correct by fastforce
next
  case (Seq c1 s1 s2 c2 s3)
  let ?cc1 = "ccomp2 c1"  let ?cc2 = "ccomp2 c2"
  have "?cc1 @ ?cc2 \<turnstile> (0,s1,stk) \<rightarrow>* (size ?cc1,s2,stk)"
    using Seq.hyps(2) by (fastforce)
  moreover
  have "?cc1 @ ?cc2 \<turnstile> (size ?cc1,s2,stk) \<rightarrow>* (size(?cc1 @ ?cc2),s3,stk)"
    using Seq.hyps(4) by (fastforce)
  ultimately show ?case
    by simp
next
  case (WhileTrue b s1 c s2 s3)
  let ?cc = "ccomp2 c"
  let ?cb = "bcomp b False (size ?cc + 1)"
  let ?cw = "ccomp2(WHILE b DO c)"
  have "?cw \<turnstile> (0,s1,stk) \<rightarrow>* (size ?cb + size ?cc,s2,stk)"
    using WhileTrue(1,3) bcomp_correct[of "size (ccomp2 c) + 1" b False] by fastforce
  moreover
  have "?cw \<turnstile> (size ?cb + size ?cc,s2,stk) \<rightarrow>* (0,s2,stk)"
    by (fastforce)
  moreover
  have "?cw \<turnstile> (0,s2,stk) \<rightarrow>* (size ?cw,s3,stk)" by(rule WhileTrue(5))
  ultimately show ?case by(blast intro: star_trans)
next
  case (IfFalse b s c2 t c1) then show ?case
    by (force intro!: bcomp_correct)
next
  case (IfTrue b s c\<^sub>1 t c\<^sub>2)
  show ?case
  proof (cases "c\<^sub>2 = SKIP")
    case True
    with IfTrue show ?thesis
      using bcomp_correct[of "size (ccomp2 c\<^sub>1)" b False s stk]
      apply simp
      apply (rule exec_append_trans)
         apply auto
      done
  next
    case False
    with IfTrue show ?thesis
      apply auto
      apply (rule exec_append_trans[OF bcomp_correct]; simp)
    apply ((rule exec_append_trans, assumption); simp)
    by fastforce
  qed
qed (fastforce intro!: bcomp_correct)+

(*<*)
end
(*>*)

