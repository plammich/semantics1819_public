theory tut04
  imports Main "HOL-IMP.Big_Step"
begin

inductive star :: "('a \<Rightarrow> 'a \<Rightarrow> bool) \<Rightarrow> 'a \<Rightarrow> 'a \<Rightarrow> bool" for r where
  refl:  "star r x x" |
  step:  "r x y \<Longrightarrow> star r y z \<Longrightarrow> star r x z"

inductive is_path :: "('v \<Rightarrow> 'v \<Rightarrow> bool) \<Rightarrow> 'v \<Rightarrow> 'v list \<Rightarrow> 'v \<Rightarrow> bool" where
  empty: "u = v \<Longrightarrow> is_path E u [] v" |
  step:  "E u v \<Longrightarrow> is_path E v ps w \<Longrightarrow> is_path E u (u # ps) w"

fun path :: "('v \<Rightarrow> 'v \<Rightarrow> bool) \<Rightarrow> 'v \<Rightarrow> 'v list \<Rightarrow> 'v \<Rightarrow> bool" where
  "path E u [] v \<longleftrightarrow> (u = v)" |
  "path E u [v] w \<longleftrightarrow> u = v \<and> E v w" |
  "path E u (v1 # v2 # ps) w \<longleftrightarrow> u = v1 \<and> E v1 v2 \<and> path E v2 (v2 # ps) w"

theorem path_is_path:
  "path E u xs v \<longleftrightarrow> is_path E u xs v"
proof
  assume "path E u xs v"
  then show "is_path E u xs v"
    by (induction E u xs v rule: path.induct) (auto intro: is_path.intros)
next
  assume "is_path E u xs v"
  then show "path E u xs v"
  proof (induction rule: is_path.inducts)
    case (empty u v E)
    then show ?case
      by auto
  next
    case (step E u v ps w)
    show ?case
    proof (cases ps)
      case Nil
      then show ?thesis
        using step by auto
    next
      case (Cons a qs)
      then show ?thesis
        using step by (auto elim: is_path.cases)
    qed
  qed
qed

end