(*<*)
theory ex04
imports Main "HOL-IMP.Big_Step"
begin
(*>*)

text \<open>\Exercise{Stars and Paths}\<close>
(*<*)
inductive star :: "('a \<Rightarrow> 'a \<Rightarrow> bool) \<Rightarrow> 'a \<Rightarrow> 'a \<Rightarrow> bool" for r where
  refl:  "star r x x" |
  step:  "r x y \<Longrightarrow> star r y z \<Longrightarrow> star r x z"
(*>*)
text \<open>
We want to write down an inductive predicate characterizing paths in directed graphs.
We will model directed graphs simply as a relation specifying the edges of type
@{typ "'v \<Rightarrow> 'v \<Rightarrow> bool"}.
The statement \<open>is_path E u p v\<close> should denote that there is path from \<open>u\<close> to \<open>v\<close>
in the graph specified by \<open>E\<close> using the vertices in \<open>p\<close>. The list of vertices \<open>p\<close> should contain
\<open>u\<close> as its first element but not \<open>v\<close> as it last element. Try to write down this predicate:
\<close>
inductive is_path :: "('v \<Rightarrow> 'v \<Rightarrow> bool) \<Rightarrow> 'v \<Rightarrow> 'v list \<Rightarrow> 'v \<Rightarrow> bool"
(*<*)
for E where
  NilI: "is_path E u [] u"
| ConsI: "\<lbrakk> E u v; is_path E v l w \<rbrakk> \<Longrightarrow> is_path E u (u#l) w"
(*>*)

text \<open>
Now also write down a recursive function \<open>path\<close> such that \<open>path E u p v\<close> checks whether \<open>p\<close> is
a valid path from \<open>u\<close> to \<open>v\<close> in \<open>E\<close>:
\<close>
fun path :: "('v \<Rightarrow> 'v \<Rightarrow> bool) \<Rightarrow> 'v \<Rightarrow> 'v list \<Rightarrow> 'v \<Rightarrow> bool" where
(*<*)
  "path E u [] v \<longleftrightarrow> v = u" |
  "path E u [v] w \<longleftrightarrow> u = v \<and> E v w" |
  "path E u (x # y # xs) v \<longleftrightarrow> (u = x \<and> E x y \<and> path E y (y # xs) v)"

lemma path_Nil: "is_path E u [] v \<longleftrightarrow> u=v"
(*
by (auto intro: is_path.intros elim: is_path.cases)
*)
proof
  assume "is_path E u [] v"
  then show "u = v"
    by (cases rule: is_path.cases) auto
next
  assume "u = v"
  then show "is_path E u [] v"
    by (auto intro: is_path.intros)
qed

lemma path_Cons: "is_path E u (v#p) w \<longleftrightarrow> 
  (\<exists>vh. u=v \<and> E v vh \<and> is_path E vh p w)"
  by (auto intro: is_path.intros elim: is_path.cases)

(*>*)
text \<open>Prove that \<open>path\<close> and \<open>is_path\<close> really represent the same specification of a path:\<close>
theorem path_is_path:
  "path E u xs v \<longleftrightarrow> is_path E u xs v"
(*<*)
proof (induction xs arbitrary: u)
  case Nil
  then show ?case
    by (auto simp: path_Nil)
next
  case (Cons x xs)
  then show ?case
    by (cases xs) (auto simp: path_Nil path_Cons)
qed
(*>*)

text \<open>It is not possible to write down a function similar to \<open>path\<close> to define the Kleene star. Why?
However, we can use a function to define \<open>E\<^sup>n\<close> for any natural number \<open>n\<close> and binary relation \<open>E\<close>,
such that \<open>(u,v)\<in>E\<^sup>n\<close> iff \<open>v\<close> can be reached from \<open>u\<close> by exactly \<open>n\<close> steps in \<open>E\<close>.
Complete its definition:
\<close>
fun star_n :: "('v \<Rightarrow> 'v \<Rightarrow> bool) \<Rightarrow> nat \<Rightarrow> 'v \<Rightarrow> 'v \<Rightarrow> bool" where
(*<*)
  "star_n _ 0 u v \<longleftrightarrow> u = v" |
  "star_n E (Suc n) u v \<longleftrightarrow> (\<exists> x. E u x \<and> star_n E n x v)"
(*>*)
text \<open>Show the following correspondence between \<open>star_n\<close> and \<open>star\<close>:\<close>
theorem star_n_star:
  "star E u v \<longleftrightarrow> (\<exists>k. star_n E k u v)"
(*<*)
proof safe
  fix k assume "star_n E k u v"
  then show "star E u v"
    by (induction E k u v rule: star_n.induct) (auto intro: star.intros)
next
  assume "star E u v"
  then show "\<exists>k. star_n E k u v"
  proof induction
    case (refl x)
    have "star_n E 0 x x"
      by simp
    then show ?case
      by blast
  next
    case (step x y z)
    then obtain k where "star_n E k y z"
      by blast
    with step.hyps have "star_n E (k + 1) x z"
      by auto
    then show ?case
      by blast
  qed
qed
(*>*)

text \<open>Try to find a similar correspondence between \<open>star_n\<close> and \<open>is_path\<close> and prove it!\<close>
(*<*)
lemma star_n_is_path:
  "star_n E k u v \<longleftrightarrow> (\<exists>xs. is_path E u xs v \<and> length xs = k)"
proof (induction k arbitrary: u)
case 0
  then show ?case
    by (auto simp: path_Nil)
next
  case (Suc k u)
  show ?case
  proof safe
    assume "star_n E (Suc k) u v"
    then obtain x where "E u x" "star_n E k x v"
      by auto
    with Suc.IH[of x] obtain xs where "is_path E x xs v" "length xs = k"
      by auto
    with \<open>E u x\<close> have "is_path E u (u # xs) v"
      by (auto simp: path_Cons)
    with \<open>_ = k\<close> show "\<exists>xs. is_path E u xs v \<and> length xs = Suc k"
      by auto
  next
    fix xs
    assume "is_path E u xs v" "length xs = Suc k"
    with Suc.IH show "star_n E (Suc k) u v"
      by (cases xs) (auto simp: path_Cons)
  qed
qed
(*>*)

(*<*)
end
(*>*)
