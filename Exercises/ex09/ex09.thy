(*<*)
theory ex09
  imports "IMP2.Examples"
begin
(*>*)

text \<open>\ExerciseSheet{9}{11.12.2018}\<close>

text \<open>\Exercise{DFS Search}\<close>
text \<open>In this exercise we will design and verify a simple DFS search through a graph.
Given some start node \<open>s\<close>, our goal will be to check whether we can reach a given target \<open>x\<close>
using the edges in the graph. For simplicity, nodes in our graph can be any integer numbers,
and the edges of the graph will be represented by a single array \<open>a\<close>, specifying the
\<^emph>\<open>successors\<close> of a node as successive integers in the array. More precisely, we define
the successors of node \<open>i\<close> in \<open>a\<close> as:
\<close>
definition succs where
  "succs a i \<equiv> a ` {i+1..<a i}" for a :: "int \<Rightarrow> int"

text \<open>Now the edge set (as a set of pairs) of the graph can simply be defined as:\<close>
definition Edges where
  "Edges a \<equiv> {(i, j). j \<in> succs a i}"
(*<*)
lemma [named_ss vcg_bb]:
  "lhsv (Inline c) = lhsv c"
  unfolding Inline_def ..

program_spec get_succs'
  assumes "j \<le> stop"
  ensures "stack ` {0..<i\<^sub>0} = stack\<^sub>0 ` {0..<i\<^sub>0}
    \<and> lran stack i\<^sub>0 i = filter (\<lambda> x. x \<notin> set_of visited) (lran a j\<^sub>0 stop)
    \<and> i \<ge> i\<^sub>0"
defines
\<open>
  while (j < stop)
  @invariant \<open>stack ` {0..<i\<^sub>0} = stack\<^sub>0 ` {0..<i\<^sub>0}
    \<and> lran stack i\<^sub>0 i = filter (\<lambda> x. x \<notin> set_of visited) (lran a j\<^sub>0 j) \<and> j \<le> stop \<and> i\<^sub>0 \<le> i \<and> j\<^sub>0 \<le> j
  \<close>
  @variant \<open>nat (stop - j)\<close>
  {
    succ = a[j];
    if (visited[succ] == 0) {
      stack[i] = succ;
      i = i + 1
    };
    j = j + 1
  }
\<close>
  apply vcg_cs
  apply auto
  by (metis atLeastLessThan_iff image_iff lran_upd_outside(2) set_lran)

program_spec get_succs
  assumes "j \<le> stop \<and> stop = a (j - 1)"
  ensures "stack ` {0..<i\<^sub>0} = stack\<^sub>0 ` {0..<i\<^sub>0}
    \<and> set (lran stack i\<^sub>0 i) = {x. (j\<^sub>0 - 1, x) \<in> Edges a \<and> x \<notin> set_of visited} \<and> i \<ge> i\<^sub>0"
  for i j stop stack[] a[] visited[]
defines \<open>inline get_succs'\<close>
  by vcg_cs (auto simp: Edges_def succs_def set_lran)

program_spec get_succs1
  assumes "j \<le> stop \<and> stop = a (j - 1) \<and> 0 \<le> i"
  ensures "
    set (lran stack 0 i) = {x. (j\<^sub>0 - 1, x) \<in> Edges a \<and> x \<notin> set_of visited} \<union> set (lran stack\<^sub>0 0 i\<^sub>0)
    \<and> i \<ge> i\<^sub>0"
  for i j stop stack[] a[] visited[]
defines \<open>inline get_succs\<close>
  apply vcg_cs

  apply auto
     apply (auto simp: succs_def set_lran)
     apply fastforce
    apply fastforce
  defer
   apply force
  apply (subst ivl_disj_un(17)[symmetric, rotated], assumption)
  apply auto
  done
(*>*)

text \<open>The set of states that are reachable from a given state \<open>s\<close> is \<open>(Edges a)\<^sup>* `` {s}\<close>.
(Click on the individual parts to understand the definition).
Now try to program a DFS search that decides \<open>x \<in> (Edges a)\<^sup>* `` {s}\<close>.
Your program should use one array as a stack to keep track of the nodes it still needs to visit and
and another array to represent the set of nodes that the search has already explored.
For now we do not concern ourselves with termination, so you can use the keyword \<open>(partial)\<close> for
your definition. Use the following outline for your code:
\<close>

text
\<open>
  \<^verbatim>\<open>b = 0;\<close>

  \<^verbatim>\<open>i = 1;\<close> \<comment>\<open>\<open>i\<close> will point to the next free space in the stack (i.e. it is the size of the stack)\<close>
  \<^verbatim>\<open>stack[0] = s;\<close> \<comment>\<open>Initially, we put \<open>s\<close> on the stack.\<close>

  \<^verbatim>\<open>while ... {
  \<close>
    \<^verbatim>\<open>i = i - 1;
    next = stack[i];\<close> \<comment>\<open>Take the top most element from the stack.\<close>

    \<comment>\<open>If it is the target, we are done. Set b = 1.\<close>

    \<comment>\<open>Else, mark \<open>next\<close> as visited,
    and put its successors on the stack if they have not been visited yet.\<close>

  \<^verbatim>\<open>}\<close>
\<close>

text \<open>Prove that your program fulfills the following specification:\<close>
program_spec (partial) dfs
  assumes "0 \<le> x \<and> 0 \<le> s"
  ensures "b = 1 \<longrightarrow> x \<in> (Edges a)\<^sup>* `` {s}"
(*<*)
defines
\<open>
  b = 0;
  \<comment>\<open>\<open>i\<close> will point to the next free space in the stack (i.e. it is the size of the stack)\<close>
  i = 1;
  \<comment>\<open>Initially, we put \<open>s\<close> on the stack.\<close>
  stack[0] = s;
  while (b == 0 \<and> i \<noteq> 0)
    @invariant \<open>0 \<le> i
    \<and> set (lran stack 0 (if b = 1 then i + 1 else i)) \<subseteq> (Edges a)\<^sup>* `` {s}
    \<and> (b = 1 \<longrightarrow> stack i = x)
    \<close>
  {
    \<comment>\<open>Take the top most element from the stack.\<close>
    i = i - 1;
    next = stack[i];
    if (next == x) {
      \<comment>\<open>If it is the target, we are done.\<close>
      b = 1
    } else {
      \<comment>\<open>Else, mark it as visited,\<close>
      visited[next] = 1;
      \<comment>\<open>and put its successors on the stack if they are not yet visited.\<close>
      stop = a[next];
      j = next + 1;
      if (j \<le> stop) {
        inline get_succs1
      }
    }
  }
\<close>
  apply vcg_cs
  subgoal
    unfolding set_lran by auto
  subgoal for s\<^sub>0 a\<^sub>0 x\<^sub>0 i stack visited i' stack'
    apply safe
    subgoal
      unfolding set_lran by (rule rtrancl_into_rtrancl; simp add: subset_eq)
    by (auto dest: in_set_butlastD)
  by (auto dest: in_set_butlastD)
(*>*)
text \<open>\<^emph>\<open>Hint\<close>:
You will likely want to factor out an inner loop for finding the successors of a node
as a subprogram.
\<close>

text \<open>Assuming that the input graph is finite, we can also prove that the algorithm terminates.
We will thus use an \<open>Isabelle context\<close> to fix a certain finite graph and a start state:
\<close>
context
  fixes start :: int and edges
  assumes finite_graph: "finite ((Edges edges)\<^sup>* `` {start})"
begin

text \<open>Prove the following specification for your program:\<close>
program_spec dfs1
  assumes "0 \<le> x \<and> 0 \<le> s \<and> start = s \<and> edges = a \<and> visited ` {x. True} = {0}"
    ensures "b = 1 \<longrightarrow> x \<in> (Edges a)\<^sup>* `` {s}"
(*<*)
defines
\<open>
  b = 0;
  \<comment>\<open>\<open>i\<close> will point to the next free space in the stack (i.e. it is the size of the stack)\<close>
  i = 1;
  \<comment>\<open>Initially, we put \<open>s\<close> on the stack.\<close>
  stack[0] = s;
  while (b == 0 \<and> i \<noteq> 0)
    @invariant \<open>0 \<le> i
    \<and> set (lran stack 0 (if b = 1 then i + 1 else i)) \<subseteq> (Edges a)\<^sup>* `` {s}
    \<and> (b = 1 \<longrightarrow> stack i = x)
    \<and> set_of visited \<subseteq> (Edges edges)\<^sup>* `` {start}
    \<close>
    @relation \<open>({(a, b). a < b} <*lex*> {(a, b). a < b}) :: ((nat \<times> nat) \<times> (nat \<times> nat)) set\<close>
    @variant \<open>(card ((Edges a)\<^sup>* `` {s}) - card (set_of visited), nat i)\<close>
  {
    \<comment>\<open>Take the top most element from the stack.\<close>
    i = i - 1;
    next = stack[i];
    if (next == x) {
      \<comment>\<open>If it is the target, we are done.\<close>
      b = 1
    } else if (visited[next] == 0) {
      \<comment>\<open>Else, mark it as visited,\<close>
      visited[next] = 1;
      \<comment>\<open>and put its successors on the stack if they are not yet visited.\<close>
      stop = a[next];
      j = next + 1;
      if (j \<le> stop) {
        inline get_succs1
      }
    }
  }
\<close>
  apply vcg_cs
  subgoal
    by (auto intro: wf_less)
  subgoal
    unfolding set_lran by auto
  subgoal for s\<^sub>0 a\<^sub>0 x\<^sub>0 i stack visited
    apply safe
    subgoal
      unfolding set_lran by (rule rtrancl_into_rtrancl; simp add: subset_eq)
    subgoal
      by (auto dest: in_set_butlastD)
    unfolding set_lran by (simp add: image_subset_iff)
  subgoal premises prems for x\<^sub>0 visited\<^sub>0 i stack visited i' stack'
  proof -
    have 1: "stack (i - 1) \<in> (Edges edges)\<^sup>* `` {start}"
      using prems unfolding set_lran by (simp add: image_subset_iff)
    have 2: "set_of visited \<subseteq> (Edges edges)\<^sup>* `` {start}"
      by (rule prems)
    from \<open>visited (stack (i - 1)) = 0\<close> have "stack (i - 1) \<notin> set_of visited"
      by auto
    then have "card (insert (stack (i - 1)) (set_of visited)) > card (set_of visited)"
      using 2 finite_graph finite_subset by fastforce
    moreover have "card (insert (stack (i - 1)) (set_of visited)) \<le> card ((Edges edges)\<^sup>* `` {start})"
      using 1 2 finite_graph by (simp add: card_mono)
    ultimately show ?thesis
      by auto
  qed
  subgoal
    apply safe
    subgoal
      by (auto dest: in_set_butlastD)
    unfolding set_lran by (simp add: image_subset_iff)
  subgoal premises prems for x\<^sub>0 visited\<^sub>0 i stack visited
  proof -
    have 1: "stack (i - 1) \<in> (Edges edges)\<^sup>* `` {start}"
      using prems unfolding set_lran by (simp add: image_subset_iff)
    have 2: "set_of visited \<subseteq> (Edges edges)\<^sup>* `` {start}"
      by (rule prems)
    from \<open>visited (stack (i - 1)) = 0\<close> have "stack (i - 1) \<notin> set_of visited"
      by auto
    then have "card (insert (stack (i - 1)) (set_of visited)) > card (set_of visited)"
      using 2 finite_graph finite_subset by fastforce
    moreover have "card (insert (stack (i - 1)) (set_of visited)) \<le> card ((Edges edges)\<^sup>* `` {start})"
      using 1 2 finite_graph by (simp add: card_mono)
    ultimately show ?thesis
      by auto
  qed
  subgoal
    by (auto dest: in_set_butlastD)
  done

end
(*>*)

(*<*)
end
(*>*)