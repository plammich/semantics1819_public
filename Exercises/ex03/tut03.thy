theory tut03
  imports "HOL-IMP.ASM"
begin

text \<open>Reflexive Transitive Closure\<close>

inductive P :: "'a \<Rightarrow> bool"

thm P.cases

lemma
  "\<not> P x"
(*
  apply (auto elim: P.cases) or:
*)
  apply (auto)
  apply (cases rule: P.cases)
  apply assumption
  done

inductive star :: "('a \<Rightarrow> 'a \<Rightarrow> bool) \<Rightarrow> 'a \<Rightarrow> 'a \<Rightarrow> bool"
  for R where
  refl: "star R x x" |
  step: "\<lbrakk>star R x y; R y z\<rbrakk> \<Longrightarrow> star R x z"

lemma
  "star R x y \<Longrightarrow> R y z \<Longrightarrow> star R x z"
  apply (rule star.intros)
   apply assumption+
  done

thm star.induct

lemma star_prepend: "\<lbrakk>star r y z; r x y\<rbrakk> \<Longrightarrow> star r x z"
  apply (induction rule: star.induct)
   apply (auto intro: star.intros)
  done

lemma star_append: "\<lbrakk> star r x y; r y z \<rbrakk> \<Longrightarrow> star r x z"
  by (rule star.intros)

inductive star' :: "('a \<Rightarrow> 'a \<Rightarrow> bool) \<Rightarrow> 'a \<Rightarrow> 'a \<Rightarrow> bool"
  for R where
    refl': "star' R x x" |
  step': "\<lbrakk>R x y; star' R y z\<rbrakk> \<Longrightarrow> star' R x z"

lemma star'_prepend:
  "\<lbrakk>star' r y z; r x y\<rbrakk> \<Longrightarrow> star' r x z"
  by (rule step')

lemma star'_append: "\<lbrakk> star' r x y; r y z \<rbrakk> \<Longrightarrow> star' r x z"
  apply (induction rule: star'.induct)
   apply (auto intro: star'.intros)
  done

lemma "star r x y \<longleftrightarrow> star' r x y"
  apply auto

   apply (induction rule: star.induct)
    apply (auto intro: star'.intros )
   apply (rule star'_append)
  apply assumption+

apply (induction rule: star'.induct)
   apply (auto intro: star.intros star_prepend)
  done


text \<open>Avoiding Stack Underflow\<close>

fun exec1 :: "instr \<Rightarrow> state \<Rightarrow> stack \<Rightarrow> stack option"
where
  "exec1 (LOADI n) _ stk  =  Some (n # stk)" |
  "exec1 (LOAD x) s stk  =  Some (s(x) # stk)" |
  "exec1  ADD _ (j # i # stk)  =  Some ((i + j) # stk)" |
  "exec1 ADD _ _ = None"

fun exec :: "instr list \<Rightarrow> state \<Rightarrow> stack \<Rightarrow> stack option"
where
  "exec [] _ stk = Some stk" |
  "exec (i#is) s stk = (case exec1 i s stk of
    Some stk' \<Rightarrow> exec is s stk'
  | None \<Rightarrow> None)"

lemma exec_append:
  "exec (is1@is2) s stk = (case exec is1 s stk of
    Some stk' \<Rightarrow> exec is2 s stk'
  | None \<Rightarrow> None)"
apply(induction is1 arbitrary: stk)
apply (auto split: option.splits)
  oops


lemma exec_append:
  "exec is1 s stk = Some stk' \<Longrightarrow> exec (is1@is2) s stk = exec is2 s stk'"
apply(induction is1 arbitrary: stk)
apply (auto split: option.splits)
  done

theorem exec_comp: "exec (comp a) s stk = Some (aval a s # stk)"
  apply(induction a arbitrary: stk)

(* apply (auto simp: exec_append) or: *)

    apply (auto)[]
   apply (auto)[]
  apply auto
  apply (subst exec_append)
   apply (auto split: option.splits)
  apply (subst exec_append)
   apply (auto split: option.splits)
  done

end