theory tut02
  imports "HOL-IMP.AExp" "HOL-IMP.BExp"
begin





fun itrev :: "'a list \<Rightarrow> 'a list \<Rightarrow> 'a list" where
  "itrev [] ys = ys"
| "itrev (x # xs) ys = itrev xs (x # ys)"

lemma itrev_rev:
  "itrev xs ys = rev xs @ ys"
  apply (induction xs arbitrary: ys)
   apply auto
  done

fun fold_left :: "('a \<Rightarrow> 'b \<Rightarrow> 'b) \<Rightarrow> 'a list \<Rightarrow> 'b \<Rightarrow> 'b" where
  "fold_left _ [] a = a"
| "fold_left f (x # xs) a = fold_left f xs (f x a)"

lemma fold_left_itrev:
  "fold_left (#) xs ys = itrev xs ys"
  apply (induction xs arbitrary: ys)
   apply auto
  done

lemma
  "fold_left (#) xs [] = rev xs"
  apply (subst fold_left_itrev)
  apply (subst itrev_rev)
  apply simp
  done

lemma fold_left_rev[simp]:
  "fold_left (#) xs ys = rev xs @ ys"
  apply (induction xs arbitrary: ys)
   apply auto
  done

lemma
  "fold_left (#) xs [] = rev xs"
  apply (subst fold_left_rev)
  apply simp
  done

fun deduplicate :: "'a list \<Rightarrow> 'a list" where
  "deduplicate [] = []" |
  "deduplicate [x] = [x]" |
  "deduplicate (x # y # xs) = (if x = y then deduplicate (y # xs) else x # deduplicate (y # xs))"

value "deduplicate [1,1,2,3,2,2,1::nat] = [1,2,3,2,1]"

lemma
  "length (deduplicate xs) \<le> length xs"
  by (induction xs rule: deduplicate.induct) auto

fun subst :: "vname \<Rightarrow> aexp \<Rightarrow> aexp \<Rightarrow> aexp" where
  "subst x a (N c) = (N c)" |
  "subst x a (V y) = (if x = y then a else V y)" |
  "subst x a (Plus a1 a2) = Plus (subst x a a1) (subst x a a2)"
term fun_upd
lemma subst_lemma: "aval (subst x a' a) s = aval a (s(x:=aval a' s))"
  apply (induction a rule: subst.induct)
    apply auto
(* single step mode:
    apply simp
   apply simp
apply (subst subst.simps)
  apply (subst aval.simps)+
  apply (simp add: fun_upd_def)
*)
  done

lemma comp: "aval a1 s = aval a2 s \<Longrightarrow> aval (subst x a1 a) s = aval (subst x a2 a) s"
  apply (subst subst_lemma)+
  apply simp
  done


datatype aexp' = N' int | V' vname | Plus' aexp' aexp' | PI' vname

term "(let (a, b) = some_result in a + b)"

fun aval' :: "aexp' \<Rightarrow> state \<Rightarrow> val \<times> state" where
  "aval' (N' n) s = (n, s)" |
  "aval' (V' x) s = (s x, s)" |
  "aval' (Plus' a\<^sub>1 a\<^sub>2) s = (
    let (x1, s1) = aval' a\<^sub>1 s in
    let (x2, s2) = aval' a\<^sub>2 s1 in
    (x1 + x2, s2)
  )"
| "aval' (PI' x) s = (s x, s(x := s x + 1))"

lemma aval'_mono:
  "aval' a s = (v, s') \<Longrightarrow> s x \<le> s' x"
  apply (induction a arbitrary: s s' v)
     apply (auto simp: null_state_def split: prod.splits)
  apply force
  done

lemma
  "aval' a <> = (v, s') \<Longrightarrow> 0 \<le> s' x"
  using aval'_mono by (fastforce simp: null_state_def)