(*<*)
theory ex10
  imports "IMP2.Examples"
begin
(*>*)
text \<open>\ExerciseSheet{10}{18.12.2018}\<close>

text \<open>\Exercise{Procedure Monotonicity}\<close>
text \<open>Prove that if we only \<^emph>\<open>add\<close> procedures to the program context,
any program execution in the old context is still valid in the new context:\<close>
theorem
  assumes "\<forall>a \<in> dom \<pi>. \<pi> a = \<pi>' a"  
  assumes "\<pi>:(c,s) \<Rightarrow> t"
  shows "\<pi>':(c,s) \<Rightarrow> t"
(*<*)
  using assms(2,1)
proof (induction \<pi> c s t rule: big_step_induct)
  case (PCall \<pi> p c s t)
  then have "\<pi>' p = Some c" "\<pi>': (c, s) \<Rightarrow> t"
    by force+
  then show ?case
    by auto
qed auto
(*>*)
text \<open>Here @{term "dom \<pi>"} denotes the domain of \<open>\<pi>\<close>, i.e. the set of procedure names in \<open>\<pi>\<close>.\<close>

text \<open>\Exercise{DFS Search, Again}\<close>
text \<open>
Consider the DFS search from last tutorial.
This time we want to refactor the program to pull out elementary operations such as \<open>push\<close> and \<open>pop\<close>
on a stack as separate procedures.
\<^item> Define \<open>push\<close> and \<open>pop\<close> and insert them in the old program
\<^item> Take a look at the existing specifications and invariants, and try to find specifications
for \<open>push\<close> and \<open>pop\<close> that will ease the proof
\<^item> Use the correct procedures that you know from the lecture for the visited set
\<^item> You may need to adjust the invariants from the old program but in the end the proofs
should become simple(r)
\<^item> Finally turn the code for computing successors into a proper procedure instead of inlining it
\<close>
(*<*)
definition succs where
  "succs a i \<equiv> a ` {i+1..<a i}" for a :: "int \<Rightarrow> int"

definition Edges where
  "Edges a \<equiv> {(i, j). j \<in> succs a i}"

procedure_spec push' (x, stack, ptr) returns (stack, ptr)
  assumes "ptr \<ge> 0" ensures \<open>lran stack 0 ptr = lran stack\<^sub>0 0 ptr\<^sub>0 @ [x\<^sub>0] \<and> ptr = ptr\<^sub>0 + 1\<close>
  defines \<open>stack[ptr] = x; ptr = ptr + 1\<close>
  by vcg_cs

procedure_spec push (x, stack, ptr) returns (stack, ptr)
  assumes "ptr \<ge> 0" ensures \<open>stack ` {0..<ptr} = {x\<^sub>0} \<union> stack\<^sub>0 ` {0..<ptr\<^sub>0} \<and> ptr = ptr\<^sub>0 + 1\<close>
  for stack[]
  defines \<open>stack[ptr] = x; ptr = ptr + 1\<close>
  by vcg_cs (auto simp: fun_upd_image)

program_spec get_succs
  assumes "j \<le> stop \<and> stop = a (j - 1) \<and> 0 \<le> i"
  ensures "
    stack ` {0..<i} = {x. (j\<^sub>0 - 1, x) \<in> Edges a \<and> x \<notin> set_of visited} \<union> stack\<^sub>0 ` {0..<i\<^sub>0}
    \<and> i \<ge> i\<^sub>0"
for i j stop stack[] a[] visited[]
defines
\<open>
  while (j < stop)
  @invariant \<open>stack ` {0..<i} = {x. x \<in> a ` {j\<^sub>0..<j} \<and> x \<notin> set_of visited} \<union> stack\<^sub>0 ` {0..<i\<^sub>0}
    \<and> j \<le> stop \<and> i\<^sub>0 \<le> i \<and> j\<^sub>0 \<le> j
  \<close>
  @variant \<open>stop - j\<close>
  {
    succ = a[j];
    is_elem = bv_elem(succ,visited);
    if (is_elem == 0) {
      (stack, i) = push (succ, stack, i)
    };
    j = j + 1
  }
\<close>
  by vcg_cs (auto simp: intvs_incr_h Edges_def succs_def)

procedure_spec pop (stack, ptr) returns (x, ptr)
  assumes "ptr \<ge> 1" ensures \<open>stack\<^sub>0 ` {0..<ptr\<^sub>0} = stack\<^sub>0 ` {0..<ptr} \<union> {x} \<and> ptr\<^sub>0 = ptr + 1\<close>
  for stack[]
  defines \<open>ptr = ptr - 1; x = stack[ptr]\<close>
  by vcg_cs (simp add: intvs_upper_decr)

procedure_spec stack_init () returns i
  assumes True ensures \<open>i = 0\<close> 
  defines \<open>i = 0\<close>
  by vcg_cs

program_spec (partial) dfs
  assumes "0 \<le> x \<and> 0 \<le> s"
  ensures "b = 1 \<longrightarrow> x \<in> (Edges a)\<^sup>* `` {s}" defines \<open>
  b = 0;
  clear stack[];
  i = stack_init();
  (stack, i) = push (s, stack, i);
  while (b == 0 \<and> i \<noteq> 0)
    @invariant \<open>0 \<le> i \<and> (
    if b = 0 then stack ` {0..<i} \<subseteq> (Edges a)\<^sup>* `` {s}
    else x \<in> (Edges a)\<^sup>* `` {s})
  \<close>
  {
    (next, i) = pop(stack, i); \<comment>\<open>Take the top most element from the stack.\<close>
    if (next == x) {
      b = 1 \<comment>\<open>If it is the target, we are done.\<close>
    } else {
      bv_insert(next, visited); \<comment>\<open>Else, mark it as visited,\<close>
      \<comment>\<open>and put its successors on the stack if they are not yet visited.\<close>
      stop = a[next];
      j = next + 1;
      if (j \<le> stop) {
        inline get_succs
      }
    }
  }
\<close>
  apply vcg_cs
  subgoal
    by force
  by (simp add: image_subset_iff)+
(*>*)

text \<open>Assuming that the input graph is finite, we can also prove that the algorithm terminates.
We will thus use an \<open>Isabelle context\<close> to fix a certain finite graph and a start state:
\<close>
context
  fixes start :: int and edges
  assumes finite_graph: "finite ((Edges edges)\<^sup>* `` {start})"
begin

text \<open>Prove the following specification for your program:\<close>
program_spec dfs1
  assumes "0 \<le> x \<and> 0 \<le> s \<and> start = s \<and> edges = a \<and> visited ` {x. True} = {0}"
    ensures "b = 1 \<longrightarrow> x \<in> (Edges a)\<^sup>* `` {s}"
  for visited[]
(*<*)
defines
\<open>
  b = 0;
  \<comment>\<open>\<open>i\<close> will point to the next free space in the stack (i.e. it is the size of the stack)\<close>
  i = 1;
  \<comment>\<open>Initially, we put \<open>s\<close> on the stack.\<close>
  stack[0] = s;
  visited = bv_init();
  while (b == 0 \<and> i \<noteq> 0)
    @invariant \<open>0 \<le> i \<and> (
    if b = 0 then stack ` {0..<i} \<subseteq> (Edges a)\<^sup>* `` {s}
    else x \<in> (Edges a)\<^sup>* `` {s})
    \<and> set_of visited \<subseteq> (Edges edges)\<^sup>* `` {start}
    \<close>
    @relation \<open>({(a, b). a < b} <*lex*> {(a, b). a < b}) :: ((nat \<times> nat) \<times> (nat \<times> nat)) set\<close>
    @variant \<open>(card ((Edges a)\<^sup>* `` {s}) - card (set_of visited), nat i)\<close>
  {
    \<comment>\<open>Take the top most element from the stack.\<close>
    (next, i) = pop(stack, i);
    if (next == x) {
      \<comment>\<open>If it is the target, we are done.\<close>
      b = 1
    } else {
      is_elem = bv_elem(next, visited);
      if (is_elem == 0) {
        \<comment>\<open>Else, mark it as visited,\<close>
        visited = bv_insert(next, visited);
        \<comment>\<open>and put its successors on the stack if they are not yet visited.\<close>
        stop = a[next];
        j = next + 1;
        if (j \<le> stop) {
          inline get_succs
        }
      }
    }
  }
\<close>
  apply vcg_cs
  subgoal
    by (auto intro: wf_less)
  subgoal
    unfolding set_lran by auto
  subgoal premises prems for x\<^sub>0 visited\<^sub>0 visited stack visiteda "next" i' visited'a i'b stack'b
  proof -
    have 1: "next \<in> (Edges edges)\<^sup>* `` {start}"
      using prems unfolding set_lran by (simp add: image_subset_iff)
    have 2: "set_of visiteda \<subseteq> (Edges edges)\<^sup>* `` {start}"
      by (rule prems)
    from \<open>visiteda next = 0\<close> have "next \<notin> set_of visiteda"
      by auto
    then have "card (insert next (set_of visiteda)) > card (set_of visiteda)"
      using 2 finite_graph finite_subset by fastforce
    moreover have "card (insert next (set_of visiteda)) \<le> card ((Edges edges)\<^sup>* `` {start})"
      using 1 2 finite_graph by (simp add: card_mono)
    ultimately show ?thesis
      by auto
  qed
subgoal premises prems for x\<^sub>0 visited\<^sub>0 visited stack visiteda "next" ia visited'a
  proof -
    have 1: "next \<in> (Edges edges)\<^sup>* `` {start}"
      using prems unfolding set_lran by (simp add: image_subset_iff)
    have 2: "set_of visiteda \<subseteq> (Edges edges)\<^sup>* `` {start}"
      by (rule prems)
    from \<open>visiteda next = 0\<close> have "next \<notin> set_of visiteda"
      by auto
    then have "card (insert next (set_of visiteda)) > card (set_of visiteda)"
      using 2 finite_graph finite_subset by fastforce
    moreover have "card (insert next (set_of visiteda)) \<le> card ((Edges edges)\<^sup>* `` {start})"
      using 1 2 finite_graph by (simp add: card_mono)
    ultimately show ?thesis
      by auto
  qed
  by (simp add: image_subset_iff)

end
(*>*)

text \<open>
\NumHomework{Be Original!}{January 8, 2019}
  (20 regular points, plus bonus points for nice submissions)

Think up a nice formalization yourself. Some inspirations:
  \<^item> You are particularly encouraged to find an interesting algorithm, to formalize
    it in the extended IMP language, and to verify with the help of the tools you've seen
    during the last weeks. Examples:
    \<^item> A completeness proof and other extensions of the DFS search
    \<^item> BFS on graphs (a proof that BFS computes \<^emph>\<open>shortest\<close> paths is particularly interesting)
    \<^item> \<open>\<dots>\<close>
  \<^item> Formalize some interesting algorithms/data structures functionally and verify them
  \<^item> Prove some interesting result about graph/automata/formal language theory
  \<^item> Formalize some results from mathematics
  \<^item> Find interesting modifications of IMP material and prove interesting properties
    about them
  \<^item> \<open>\<dots>\<close>

You should set yourself a time limit before starting your project.
Also incomplete/unfinished formalizations are welcome and will be graded!

Please comment your formalization well, such that we can see what it
does/is intended to do.

Do not start in the last two days or you will not succeed!

You are welcome to discuss your plans with the tutor before starting
your project.
\<close>

(*<*)
end
(*>*)