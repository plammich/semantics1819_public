theory tut01
  imports Main
begin

value "2 + (2::int)"

value "(4 :: int) - 5"

value "(3::nat)*4 - 2 *(7 +1)"

lemma
  "(a :: nat) + 0 = a"
  by (rule Nat.add_0_right)

lemma commutativity:
  "(a :: nat) + b = b + a"
  by simp

lemma associativity:
  fixes a b c :: nat
  shows "a + b + c = a + (b + c)"
  by simp

fun count :: "'a list \<Rightarrow> 'a \<Rightarrow> nat" where
  "count [] _ = 0"
| "count (x # xs) y = (if x = y then 1 + count xs y else count xs y)"

value "count [1,2,2,3] (1::nat)"

lemma
  "count [1,2,2,3] (1 :: nat) = 1"
  by simp

thm count.simps

lemma
  "count xs x \<le> length xs"
  by (induction xs) auto

lemma count_singleton[simp]:
  "count [x] x = 1"
  by simp



fun snoc :: "'a list \<Rightarrow> 'a \<Rightarrow> 'a list" where
  "snoc [] x = [x]"
| "snoc (x # xs) y = x # snoc xs y"

value "snoc [] (3 :: nat) = [3]"

value "snoc [1,2] (3 :: nat) = [1,2,3]"

lemma "snoc [1,2] (3 :: nat) = [1,2,3]"
  by simp

fun reverse :: "'a list \<Rightarrow> 'a list" where
  "reverse [] = []"
| "reverse (x # xs) = snoc (reverse xs) x"

value "reverse [1,2,3::nat]"

lemma reverse_snoc[simp]:
  "reverse (snoc xs x) = x # reverse xs"
  by (induction xs) auto

lemma
  "reverse (reverse xs) = xs"
  by (induction xs) auto



end
