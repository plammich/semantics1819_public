(*<*)
theory Solution
imports "IMP2.Examples"
begin
(*>*)

text \<open>\ExerciseSheet{15}{05.~02.~2019}\<close>

text \<open>\Exercise{Program Verification} (Pen \& Paper)\<close>

paragraph \<open>Solution\<close>

text \<open>Aux lemma:\<close>
lemma lran_eq_replicate_conv: "lran a l h = replicate n x \<longleftrightarrow> (\<forall>i\<in>{l..<h}. a i = x) \<and> n=nat (h-l)"
  apply (auto simp: list_eq_iff_nth_eq)
  using zle_iff_zadd by auto

program_spec check_anbn 
  assumes "0\<le>h"
  ensures "(i>j) \<longleftrightarrow> (\<exists>n. lran a 0 h = replicate n 0 @ replicate n 1)"
  defines \<open>
    i = 0;
    j = h - 1;
    while (i<j \<and> a[i] == 0 \<and> a[j] == 1) 
      @variant \<open>j\<close>
      @invariant \<open>0\<le>i \<and> j<h \<and> i = h-1 - j \<and> i - 1 \<le> j
        \<and> lran a 0 i = replicate (nat i) 0 
        \<and> lran a (j+1) h = replicate (nat i) 1
        \<close>
    {
      i=i+1; 
      j=j-1
    }
  \<close>
  supply [simp del] = lran_tail
  apply vcg_cs
  subgoal by (clarsimp simp: lran_eq_replicate_conv Ball_def) smt
  subgoal premises prems for a h j
  proof -  
    let ?i="h - 1 - j"
    
    consider "?i = j" | "?i=j+1" | "?i<j" "a ?i \<noteq> 0" | "?i<j" "a j \<noteq> 1"
      using prems(2-4) by linarith
    
    then show ?thesis proof cases
      case 1
      hence [simp]: "h = 2*j + 1" by auto
      have "\<nexists> n. lran a 0 h = replicate n 0 @ replicate n 1"
      proof (rule_tac ccontr; clarsimp)
        fix n assume "lran a 0 (2 * j + 1) = replicate n 0 @ replicate n 1"
        then have "nat (2 * j + 1) = n + n"
          by - (drule arg_cong[where f=length], simp)
        then show False
          using \<open>?i = j\<close> \<open>j < h\<close> by (smt int_nat_eq of_nat_add)
      qed
      then show ?thesis by auto
    next
      case 2
      let ?n = "nat ?i"
      from \<open>j < h\<close> \<open>h - 2 \<le> 2 * j\<close> have "lran a 0 h = lran a 0 (j + 1) @ lran a (j + 1) h"
        by (simp add: lran_split)
      also have "\<dots> = replicate ?n 0 @ replicate ?n 1"
        using prems(3,4,5,6) 2 by (simp add: lran_split)
      finally show ?thesis
        using 2 by auto
    next
      case 3
      have "?i \<ge> 0"
        using \<open>j < h\<close> by simp
      have "\<nexists>n. lran a 0 h = replicate n 0 @ replicate n 1"
      proof (rule_tac ccontr; clarsimp)
        fix n assume A: "lran a 0 h = replicate n 0 @ replicate n 1"
        then have "nat h = n + n"
          by - (drule arg_cong[where f=length], simp)
        text \<open>Just stating that you use \<open>?i \<ge> 0\<close>, \<open>?i < j\<close>, and \<open>j < h\<close> would be enough here.\<close>
        have B: "lran a 0 h = lran a 0 ?i @ a ?i # lran a (?i + 1) h"
          apply (subst lran_split[where p = ?i])
          subgoal
            using \<open>?i \<ge> 0\<close> .
          subgoal
            using \<open>?i < j\<close> \<open>?i \<ge> 0\<close> by simp
          by (smt lran_prepend1 \<open>j < h\<close> \<open>?i < j\<close>)
        have "nat ?i \<ge> n"
        text \<open>You do not need to provide the following justification in an exam\<close>
        proof (rule ccontr)
          assume "\<not> n \<le> nat (h - 1 - j)"
          with \<open>?i \<ge> 0\<close> have "n > nat ?i"
            by auto
          with A B show False
            by (clarsimp simp: list_eq_iff_nth_eq nth_append)
               (metis \<open>a ?i \<noteq> 0\<close> \<open>0 \<le> ?i\<close> \<open>nat ?i < n\<close> int_eq_iff nth_replicate trans_less_add1)
        qed
        moreover have "2 * ?i < h"
        proof -
          have "2 * ?i < h \<longleftrightarrow> 2 * ?i < ?i + j + 1"
            by simp
          also have "\<dots> \<longleftrightarrow> ?i < j + 1"
            by (simp add: algebra_simps)
          also have "\<dots> \<longleftrightarrow> True"
            using \<open>?i < j\<close> by simp
          finally show ?thesis
            by simp
        qed
        ultimately show False
          using \<open>nat h = n + n\<close> \<open>j < h\<close> by (auto simp add: algebra_simps)
      qed
      with 3 show ?thesis
        by auto
    next
      case 4
      text \<open>An informal proof would look similar to case \<open>3\<close>.\<close>
      then show ?thesis using prems
        apply (clarsimp simp: list_eq_iff_nth_eq nth_append)
        apply (rule_tac exI[where x="nat j"])
        apply auto
        done
    qed
  qed
  done

text \<open>
  \Exercise{Hoare-Logic} (Pen \& Paper)
\<close>

text \<open>Solution:

  \<^enum> No. Consider \<open>t \<noteq> t'\<close>, and \<open>R = UNIV\<close>. Then \<open>(c,s) \<Rightarrow> t\<close> and \<open>(c,s) \<Rightarrow> t'\<close> for any \<open>s\<close>.
  \<^enum> \<open>wlp (REL R) Q s = (\<forall>t. (s, t) \<in> R \<longrightarrow> Q t)\<close>
  \<^enum>
    \<^descr>[Soundness] We first show \<open>(REL R, s) \<Rightarrow> t \<longleftrightarrow> (s, t) \<in> R\<close>. In the \<open>\<longrightarrow>\<close>-direction this follows
    by rule inversion on the big step, and in the \<open>\<longleftarrow>\<close>-direction we use rule \<open>Rel\<close>.
    Now \begin{align*}
      & & \<open>wlp (REL R) Q s\<close> \\
      & \<open>\<longleftrightarrow>\<close> & \<open>(\<forall>t. (REL R, s) \<Rightarrow> t \<longrightarrow> Q t)\<close> \\
      & \<open>\<longleftrightarrow>\<close> & \<open>(\<forall>t. (s, t) \<in> R \<longrightarrow> Q t)\<close>
    \end{align*}
    \<^descr>[Completeness] We need to show \<open>HT_partial (wlp c Q) (REL R) Q\<close>.
    \begin{align*}
      & & \<open>HT_partial (wlp c Q) (REL R) Q\<close> \\
      & \<open>\<longleftrightarrow>\<close> & \<open>(\<forall>s. wlp c Q s \<longrightarrow> wlp c Q s)\<close> \\
      & \<open>\<longleftrightarrow>\<close> & \<open>True\<close>
    \end{align*}
\<close>

(*<*)
end
(*>*)